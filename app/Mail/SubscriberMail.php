<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscriberMail extends Mailable
{
    use Queueable, SerializesModels;

    public $view;
    private $email;
    private $code;

    /**
     * Create a new message instance.
     *
     * @param $view
     * @param $email
     * @param $code
     */
    public function __construct($view, $email, $code = false)
    {
        $this->view = $view;
        $this->email = $email;
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return SubscriberMail|bool
     */
    public function build()
    {
        try {
            return $this
                        ->from(env('MAIL_FROM'))
                        ->view($this->view)
                        ->with([
                            "code" => $this->code,
                            "email" => $this->email
                        ]);
        } catch (\Exception $e) {
            return false;
        }
    }
}
