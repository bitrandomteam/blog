<?php

namespace App;

use App\Http\Traits\CategoriesRelation;
use App\Http\Traits\Errors;
use App\Http\Traits\ImagesRelation;
use App\Http\Traits\IpAddressesRelation;
use App\Http\Traits\Settings;
use App\Http\Traits\TagsRelation;
use App\Http\Traits\TemplatesRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Post extends Model
{
    use Settings;
    use ImagesRelation;
    use CategoriesRelation;
    use TemplatesRelation;
    use IpAddressesRelation;
    use TagsRelation;

    protected $fillable = [
        "header", "slug", "seo_title", "seo_keywords", "seo_description", "description",
        "publish_time", "unpublish_time", "seo_title", "category_id", "text"
    ];

    /**
     * Check input parameters.
     *
     * @param Request $request
     * @param string $exceptId
     * @return bool
     */
    public function isValid(Request $request, $exceptId = '')
    {
        if ($exceptId) {
            $exceptId = ",{$exceptId}";
        }

        $validator = Validator::make($request->all(), [
            'header' => 'required|max:255',
            'slug' => "required|max:191|alpha_dash|unique:posts,slug{$exceptId}",
            'seo_title' => 'max:255|nullable',
            'seo_keywords' => 'max:500|nullable',
            'seo_description' => 'max:500|nullable',
            'description' => 'max:10000|nullable',
            'text' => 'required|max:100000',
            'recent_posts_count' => 'integer|nullable',
            'featured_posts_count' => 'integer|nullable',
            'tags_count' => 'integer|nullable',
            'categories_count' => 'integer|nullable',
            'template_id' => 'required|integer'
        ]);

        return $validator;
    }

    /**
     * Return published recent posts.
     *
     * @param $postsCount
     * @return bool
     */
    public static function getRecentPosts($postsCount)
    {
        return Post::select('posts.*')
            ->join('categories', 'categories.id', '=', 'posts.category_id')
            ->where("posts.is_published", "=", 1)
            ->where(function ($query) {
                return DB::table('posts')
                    ->whereRaw("posts.publish_time <= NOW()")
                    ->orWhereNull("posts.publish_time");
            })
            ->where(function ($query) {
                return DB::table('posts')
                    ->whereRaw("posts.unpublish_time > NOW()")
                    ->orWhereNull("posts.unpublish_time");
            })
            ->where("categories.is_published", "=", 1)
            ->where(function ($query) {
                return DB::table('posts')
                    ->whereRaw("categories.publish_time <= NOW()")
                    ->orWhereNull("categories.publish_time");
            })
            ->where(function ($query) {
                return DB::table('posts')
                    ->whereRaw("categories.unpublish_time > NOW()")
                    ->orWhereNull("categories.unpublish_time");
            })
            ->orderBy('posts.created_at')
            ->take($postsCount)
            ->get();
    }

    /**
     * Return published posts.
     *
     * @param $postsCount
     * @return bool
     */
    public static function getPosts($postsCount)
    {
        return Post::select('posts.*')
            ->join('categories', 'categories.id', '=', 'posts.category_id')
            ->where("posts.is_published", "=", 1)
            ->where(function ($query) {
                return DB::table('posts')
                    ->whereRaw("posts.publish_time <= NOW()")
                    ->orWhereNull("posts.publish_time");
            })
            ->where(function ($query) {
                return DB::table('posts')
                            ->whereRaw("posts.unpublish_time > NOW()")
                            ->orWhereNull("posts.unpublish_time");
            })
            ->where("categories.is_published", "=", 1)
            ->where(function ($query) {
                return DB::table('posts')
                    ->whereRaw("categories.publish_time <= NOW()")
                    ->orWhereNull("categories.publish_time");
            })
            ->where(function ($query) {
                return DB::table('posts')
                    ->whereRaw("categories.unpublish_time > NOW()")
                    ->orWhereNull("categories.unpublish_time");
            })
            ->orderBy('posts.created_at')
            ->paginate($postsCount);
    }
}
