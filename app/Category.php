<?php

namespace App;

use App\Http\Traits\Errors;
use App\Http\Traits\ImagesRelation;
use App\Http\Traits\PostsRelation;
use App\Http\Traits\Settings;
use App\Http\Traits\TemplatesRelation;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Category extends Model
{
    use Settings;
    use TemplatesRelation;
    use PostsRelation;
    use ImagesRelation;

    protected $fillable = [
        "header", "slug", "seo_title", "seo_keywords", "seo_description", "description",
        "publish_time", "unpublish_time", "seo_title", "parent_id"
    ];

    /**
     * Return published categories, which hasn`t parent.
     *
     * @param $categoriesCount
     * @return Category
     */
    public static function getCategories($categoriesCount)
    {
        return Category::where("is_published", "=", 1)
            ->where(function ($query) {
                $query->whereRaw("publish_time <= NOW()")
                    ->orWhereNull("publish_time");
            })
            ->where(function ($query) {
                $query->whereRaw("unpublish_time > NOW()")
                    ->orWhereNull("unpublish_time");
            })
            ->where("type", "!=", "system")
            ->whereNull("parent_id")
            ->inRandomOrder()
            ->take($categoriesCount)
            ->get();
    }

    /**
     * Check input parameters.
     *
     * @param Request $request
     * @param string $exceptId
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function isValid(Request $request, $exceptId = '')
    {
        if ($exceptId) {
            $exceptId = ",{$exceptId}";
        }

        $validator = Validator::make($request->all(), [
            'header' => 'required|max:255',
            'slug' => "required|max:191|alpha_dash|unique:categories,slug{$exceptId}",
            'seo_title' => 'max:255|nullable',
            'seo_keywords' => 'max:500|nullable',
            'seo_description' => 'max:500|nullable',
            'description' => 'max:10000|nullable',
            'parent_id' => 'required',
            'posts_count_on_page' => 'integer|nullable',
            'recent_posts_count' => 'integer|nullable',
            'featured_posts_count' => 'integer|nullable',
            'tags_count' => 'integer|nullable',
            'subcategories_count' => 'integer|nullable',
            'template_id' => 'required|integer'
        ]);

        return $validator;
    }

    /**
     * Return category parent.
     *
     * @return BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo("App\Category", "parent_id", "id");
    }

    /**
     * Return all children of category.
     *
     * @return HasMany
     */
    public function children()
    {
        return $this->hasMany("App\Category", "parent_id", "id");
    }
}
