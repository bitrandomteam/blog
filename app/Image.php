<?php

namespace App;

use App\Http\Traits\CategoriesRelation;
use App\Http\Traits\Errors;
use App\Http\Traits\PagesRelation;
use App\Http\Traits\PostsRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use stdClass;

class Image extends Model
{
    use PagesRelation;
    use PostsRelation;
    use CategoriesRelation;

    protected $fillable = ["header", "alt", "filepath"];

    public static function validateImages(Request $request)
    {
        $validatedImages = [];
        if (isset($request->images)) {
            foreach ($request->images as $image) {
                if ($image = Image::find($image)) {
                    $validatedImages[] = $image;
                }
            }
        }

        return $validatedImages;
    }

    /**
     * Check input parameters.
     *
     * @param array $parameters
     * @param string $exceptId
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function isValid($parameters, $exceptId = '')
    {
        if ($exceptId) {
            $exceptId = ",{$exceptId}";
        }

        $validator = Validator::make($parameters, [
            'header' => "required|max:255|unique:images{$exceptId}",
            'alt' => 'required|max:1000'
        ]);

        return $validator;
    }
}
