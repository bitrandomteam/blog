<?php

namespace App;

use App\Http\Traits\PostsRelation;
use App\Http\Traits\Settings;
use App\Http\Traits\TemplatesRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Tag extends Model
{
    use Settings;
    use PostsRelation;
    use TemplatesRelation;

    protected $fillable = [
        "header", "slug", "seo_title", "seo_keywords", "seo_description", "description", "settings"
    ];

    public static function validateTags(Request $request)
    {
        $validatedTags = [];
        if (isset($request->tags)) {
            foreach ($request->tags as $tagId) {
                if ($tag = Tag::find($tagId)) {
                    $validatedTags[] = $tag;
                }
            }
        }

        return $validatedTags;
    }

    /**
     * Check input parameters.
     *
     * @param Request $request
     * @param string $exceptId
     * @return bool
     */
    public function isValid(Request $request, $exceptId = '')
    {
        if ($exceptId) {
            $exceptId = ",{$exceptId}";
        }

        $validator = Validator::make($request->all(), [
            'header' => 'required|max:255',
            'slug' => "required|max:191|alpha_dash|unique:tags,slug{$exceptId}",
            'seo_title' => 'max:255|nullable',
            'seo_keywords' => 'max:500|nullable',
            'seo_description' => 'max:500|nullable',
            'description' => 'max:10000|nullable',
            'template_id' => 'required|integer',
            'posts_count_on_page' => 'integer|nullable',
            'recent_posts_count' => 'integer|nullable',
            'featured_posts_count' => 'integer|nullable',
            'tags_count' => 'integer|nullable',
            'categories_count' => 'integer|nullable',
        ]);

        return $validator;
    }

    /**
     * Return tags.
     *
     * @param $tagsCount
     * @return Category
     */
    public static function getTags($tagsCount)
    {
        return Tag::inRandomOrder()->take($tagsCount)->get();
    }
}
