<?php

namespace App;

use App\Http\Traits\PagesRelation;
use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    use PagesRelation;

    protected $fillable = ['name', 'filename'];
}
