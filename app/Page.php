<?php

namespace App;

use App\Http\Traits\Errors;
use App\Http\Traits\ImagesRelation;
use App\Http\Traits\IpAddressesRelation;
use App\Http\Traits\Settings;
use App\Http\Traits\TemplatesRelation;
use App\Http\Traits\Views;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Page extends Model
{
    use Settings;
    use ImagesRelation;
    use TemplatesRelation;
    use IpAddressesRelation;
    use Views;

    protected $fillable = [
        "header", "slug", "seo_title", "seo_keywords", "seo_description",
        "publish_time", "unpublish_time", "seo_title", "text", "snow_in_menu"
    ];

    /**
     * Return all published pages.
     *
     * @return Page
     */
    public static function getPages()
    {
        return Page::where("is_published", "=", 1)
                    ->where(function ($query) {
                        $query->whereRaw("publish_time <= NOW()")
                               ->orWhereNull("publish_time");
                    })
                    ->where(function ($query) {
                        $query->whereRaw("unpublish_time > NOW()")
                            ->orWhereNull("unpublish_time");
                    })
                    ->orderBy("menu_ordering")
                    ->get();
    }

    /**
     * Check input parameters.
     *
     * @param Request $request
     * @param string $exceptId
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function isValid(Request $request, $exceptId = '')
    {
        if ($exceptId) {
            $exceptId = ",{$exceptId}";
        }

        $validator = Validator::make($request->all(), [
            'header' => 'required|max:255',
            'slug' => "required|max:191|alpha_dash|unique:pages,slug{$exceptId}",
            'seo_title' => 'max:255|nullable',
            'seo_keywords' => 'max:500|nullable',
            'seo_description' => 'max:500|nullable',
            'text' => 'max:100000',
            'posts_count_on_page' => 'integer|nullable',
            'recent_posts_count' => 'integer|nullable',
            'featured_posts_count' => 'integer|nullable',
            'tags_count' => 'integer|nullable',
            'categories_count' => 'integer|nullable',
            'template_id' => 'required|integer'
        ]);

        return $validator;
    }
}
