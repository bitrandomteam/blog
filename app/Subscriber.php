<?php

namespace App;

use App\Http\Traits\Errors;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class Subscriber extends Model
{
    use Errors;

    protected $fillable = ['email'];

    public static function getSubscriber($code, $email)
    {
        return Subscriber::where("code", "=", $code)->where('email', "=", $email)->first();
    }

    public static function getCode()
    {
        return Str::random(60);
    }

    public function checkDate()
    {
        $currentDate = Carbon::now();
        $codeDate = Carbon::createFromFormat("Y-m-d H:i:s", $this->updated_at);

        if ($currentDate->diffInDays($codeDate) > 1) {
            $this->code = self::getCode();
            $this->save();

            return false;
        } else {
            return true;
        }
    }

    /**
     * Check input parameters.
     *
     * @param Request $request
     * @param string $exceptId
     * @return bool
     */
    public function isValid(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
            'email' => 'required|email:rfc|max:60|unique:subscribers'
            ],
            [
                'required' => "Электронная почта не введена или введена некорректно.",
                'email' => "Электронная почта введена некорректно",
                'max' => "Электронная почта слишком длинная (максимум :size символов)",
                'unique' => "Данная почта уже получает наши еженедельные сообщения"
            ]
        );

        $this->errors = $validator;

        return !$validator->fails();
    }
}
