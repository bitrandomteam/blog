<?php


namespace App\Http\Traits;


trait Settings
{
    /**
     * Set count of posts on one page.
     *
     * @param $count
     * @return void
     */
    public function setPostsCountOnPage($count)
    {
        $this->settings->posts_count_on_page = (int)$count;
    }

    /**
     * Set count of recent posts on a page.
     *
     * @param $count
     * @return void
     */
    public function setRecentPostsCount($count)
    {
        $this->settings->recent_posts_count = (int)$count;
    }

    /**
     * Set count of featured posts on a page.
     *
     * @param $count
     * @return void
     */
    public function setFeaturedPostsCount($count)
    {
        $this->settings->featured_posts_count = (int)$count;
    }

    /**
     * Set count of tags of a page.
     *
     * @param $count
     * @return void
     */
    public function setTagsCount($count)
    {
        $this->settings->tags_count = (int)$count;
    }

    /**
     * Set count of subcategories.
     *
     * @param $count
     * @return void
     */
    public function setSubcategoriesCount($count)
    {
        $this->settings->subcategories_count = (int)$count;
    }

    /**
     * Set count of categories.
     *
     * @param $count
     * @return void
     */
    public function setCategoriesCount($count)
    {
        $this->settings->categories_count = (int)$count;
    }

    /**
     * Check and set all settings.
     *
     * @param $parameters
     * @return void
     */
    public function setSettings($parameters)
    {
        $this->settings = new \stdClass();

        if (isset($parameters->posts_count_on_page)) {
            $this->setPostsCountOnPage($parameters->posts_count_on_page);
        } else {
            $this->setPostsCountOnPage(POSTS_COUNT_ON_PAGE);
        }

        if (isset($parameters->recent_posts_count)) {
            $this->setRecentPostsCount($parameters->recent_posts_count);
        } else {
            $this->setRecentPostsCount(RECENT_POSTS_COUNT);
        }

        if (isset($parameters->featured_posts_count)) {
            $this->setFeaturedPostsCount($parameters->featured_posts_count);
        } else {
            $this->setFeaturedPostsCount(FEATURED_POSTS_COUNT);
        }

        if (isset($parameters->tags_count)) {
            $this->setTagsCount($parameters->tags_count);
        } else {
            $this->setTagsCount(TAGS_COUNT);
        }

        if (isset($parameters->subcategories_count)) {
            $this->setSubcategoriesCount($parameters->subcategories_count);
        } else {
            $this->setSubcategoriesCount(SUBCATEGORIES_COUNT);
        }

        if (isset($parameters->categories_count)) {
            $this->setCategoriesCount($parameters->categories_count);
        } else {
            $this->setCategoriesCount(CATEGORIES_COUNT);
        }
    }

    /**
     * Prepare settings to save into database.
     *
     * @param $parameters
     * @return void
     */
    public function updateSettings($parameters)
    {
        $parameters = (object)$parameters;
        $this->setSettings($parameters);
        $this->prepareSettings();
    }
    /**
     * Encode setting object to json.
     *
     * @return void
     */
    public function prepareSettings()
    {
        try {
            $this->settings = json_encode($this->settings);
        } catch (\Exception $e) {
            $this->settings = '';
        }
    }

    /**
     * Decode settings from json to object.
     *
     * @return void
     */
    public function parseSettings()
    {
        $this->settings = json_decode($this->settings);
    }
}
