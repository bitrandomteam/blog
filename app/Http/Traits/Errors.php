<?php


namespace App\Http\Traits;


use Illuminate\Support\MessageBag;

trait Errors
{
    protected $errors;

    public function __construct()
    {
        $this->errors = new MessageBag;
    }

    /**
     * Return all errors.
     *
     * @return MessageBag
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
