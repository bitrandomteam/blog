<?php


namespace App\Http\Traits;


use App\Post;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait PostsRelation
{
    /**
     * Return posts.
     *
     * @return HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function postsMany()
    {
        return $this->belongsToMany(Post::class, "post_tag", "tag_id");
    }
}
