<?php


namespace App\Http\Traits;


use Carbon\Carbon;
use Illuminate\Http\Request;

trait RequestHelper
{
    protected function updateShowInMenu(Request $request, $object)
    {
        if ($request->show_in_menu == "on") {
            $object->show_in_menu = 1;
        } else {
            $object->show_in_menu = 0;
        }
    }

    protected function updatePublish(Request $request, $object)
    {
        if ($request->is_published == "on") {
            $object->is_published = 1;
        } else {
            $object->is_published = 0;
        }
    }

    protected function updateParent(Request $request, $object)
    {
        if ($request->parent_id === "null") {
            $object->parent_id = null;
        }
    }

    protected function updateCategory(Request $request, $object)
    {
        if ($object->category_id == "null") {
            $object->category_id = null;
        }
    }

    protected function updateDate(Request $request, $object)
    {
        if (!isset($request->publish_date) && !isset($request->publish_time)) {
            $object->publish_time = null;
        } else {
            $object->publish_time = new Carbon($request->publish_date . " " . $request->publish_time);
        }

        if (!isset($request->unpublish_date) && !isset($request->unpublish_time)) {
            $object->unpublish_time = null;
        } else {
            $object->unpublish_time = new Carbon($request->unpublish_date . " " . $request->unpublish_time);
        }
    }
}
