<?php


namespace App\Http\Traits;


use App\IpAddress;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait IpAddressesRelation
{
    /**
     * Return ip-addresses.
     *
     * @return HasMany
     */
    public function ipAddresses()
    {
        return $this->belongsToMany(IpAddress::class)->withTimestamps();
    }
}
