<?php


namespace App\Http\Traits;


use App\Image;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use stdClass;

trait ImagesRelation
{
    /**
     * Return all images.
     *
     * @return BelongsToMany
     */
    public function images()
    {
        return $this->belongsToMany(Image::class);
    }

    /**
     * Check and return image object.
     *
     * @return stdClass
     */
    public function getImage()
    {
        if ($this->images()->first()) {
            return $this->images()->first();
        } else {
            $image = new stdClass();
            $image->id = null;
            $image->filepath = DEFAULT_IMAGE_FILEPATH;
            $image->alt = DEFAULT_IMAGE_ALT;

            return $image;
        }
    }
}
