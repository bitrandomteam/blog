<?php


namespace App\Http\Traits;

use App\IpAddress;

trait Views
{
    /**
     * Increment view based on user ip-address.
     *
     * @param $ip
     * @return bool
     */
    public function incrementView($ip)
    {
        foreach ($this->ipAddresses()->get() as $address) {
            if ($address->ip_address === $ip) {
                return true;
            }
        }

        if ($ipAddress = IpAddress::where("ip_address", "=", $ip)->first()) {
            $this->ipAddresses()->save($ipAddress);
        } else {
            $ipAddress = new IpAddress;
            $ipAddress->ip_address = $ip;
            $this->ipAddresses()->save($ipAddress);
        }

        $this->views++;
        $this->save();
    }

    /**
     * @param $category
     * @return string
     */
    private function getView($object)
    {
        return 'public.' . $object->template()->first()->filename;
    }
}
