<?php


namespace App\Http\Traits;


use App\Template;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait TemplatesRelation
{
    /**
     * Return template.
     *
     * @return BelongsTo
     */
    public function template()
    {
        return $this->belongsTo(Template::class);
    }
}
