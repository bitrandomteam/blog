<?php


namespace App\Http\Traits;


use App\Category;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait CategoriesRelation
{
    /**
     * Return categories.
     *
     * @return HasMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * Return category.
     *
     * @return HasMany
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
