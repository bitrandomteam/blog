<?php


namespace App\Http\Traits;


use App\Tag;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait TagsRelation
{
    /**
     * Return tags.
     *
     * @return HasMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
