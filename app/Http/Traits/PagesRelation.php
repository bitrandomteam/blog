<?php


namespace App\Http\Traits;


use App\Page;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait PagesRelation
{
    /**
     * Return pages.
     *
     * @return HasMany
     */
    public function pages()
    {
        return $this->belongsToMany(Page::class);
    }
}
