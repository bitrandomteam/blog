<?php
    define("POSTS_COUNT_ON_PAGE", 10);
    define("RECENT_POSTS_COUNT", 4);
    define("FEATURED_POSTS_COUNT", 3);
    define("TAGS_COUNT", 8);
    define("CATEGORIES_COUNT", 5);
    define("SUBCATEGORIES_COUNT", 5);

    define("DEFAULT_IMAGE_FILEPATH", "images/no-photo.jpg");
    define("DEFAULT_IMAGE_ALT", "No photo");
    define("DB_PREFIX", env("DB_PREFIX", ""));
