<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Traits\Errors;
use App\Http\Traits\RequestHelper;
use App\Http\Traits\Views;
use App\Post;
use App\Tag;
use App\Template;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\MessageBag;
use Carbon\Carbon;
use App\Page;
use App\Image;

class PagesController extends Controller
{
    use Errors;
    use RequestHelper;
    use Views;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view("admin.pages.index", [
            "pages" => Page::orderBy("id", "ASC")->paginate(15)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view("admin.pages.create", [
            "images" => Image::orderBy("id", "ASC")->get(),
            "templates" => Template::get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $page = new Page;

        $validation = $page->isValid($request);
        if (!$validation->fails()) {
            $page->fill($request->all());
            $page->updateSettings($request->all());
            $this->updateShowInMenu($request, $page);
            $this->updatePublish($request, $page);

            try {
                $this->updateDate($request, $page);
            } catch (\Exception $e) {
                $this->errors->add("500", "Дата публкации/снятия с публикации введена некорректно");
                return back()->withInput()->withErrors($this->errors);
            }

            $validatedImages = Image::validateImages($request);
            if (!$template = Template::find($request->template_id)) {
                $this->errors->add('404', "Шаблон #{$request->template_id} не был найден");
                return back()->withInput()->withErrors($this->errors);
            }

            $page->template()->associate($template);
            $page->images()->detach();
            $page->images()->saveMany($validatedImages);
            $page->save();

            return redirect()->route("pages.index")->with("status", "Страница #{$page->id} успешно создана");
        } else {
            return back()->withInput()->withErrors($validation->errors()->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param string $slug
     * @return Response
     */
    public function show(Request $request, $slug = "main")
    {
        $page = Page::where("slug", "=", $slug)->first();

        if (!$page) {
            return abort(404);
        }
        $page->incrementView($request->ip());
        $page->parseSettings();

        try {
            return view($this->getView($page), [
                "page" => $page,
                "pages" => Page::getPages(),
                "categories" => Category::getCategories($page->settings->categories_count),
                "posts" => Post::getPosts($page->settings->posts_count_on_page),
                "recent_posts" => Post::getRecentPosts($page->settings->recent_posts_count),
                "tags" => Tag::getTags($page->settings->tags_count)
            ]);
        } catch (\Exception $e) {
            return abort(404);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $page = Page::find($id);

        if (!$page) {
            $this->errors->add("404", "Страница #{$id} не найдена");
            return back()->withErrors($this->errors);
        } else {
            $page->parseSettings();
            return view("admin.pages.edit", [
                "page" => $page,
                "images" => Image::orderBy("id", "ASC")->get(),
                "templates" => Template::get()
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $page = Page::find($id);

        if (!$page) {
            $this->errors->add("404", "Страница не найдена");
            return back()->withInput()->withErrors($this->errors);
        }

        $validation = $page->isValid($request, $id);
        if (!$validation->fails()) {
            $page->fill($request->all());
            $page->updateSettings($request->all());
            $this->updateShowInMenu($request, $page);
            $this->updatePublish($request, $page);

            try {
                $this->updateDate($request, $page);
            } catch (\Exception $e) {
                $this->errors->add("500", "Время публикации/снятия с публикации введено некорректно");
                return back()->withInput()->withErrors($this->errors);
            }

            $validatedImages = Image::validateImages($request);

            if (!$template = Template::find($request->template_id)) {
                $this->errors->add('404', "Шаблон #{$request->template_id} не был найден");
                return back()->withInput()->withErrors($this->errors);
            }

            $page->template()->associate($template);
            $page->images()->detach();
            $page->images()->saveMany($validatedImages);
            $page->save();

            return redirect()->route("pages.index")->with("status", "Страница #{$page->id} успешно обновлена");
        } else {
            return back()->withInput()->withErrors($validation->errors()->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $page = Page::find($id);

        if (!$page) {
            $this->errors->add("404", "Страница #$id не была найдена");
            return back()->withErrors($this->errors);
        } else {
            $page->delete();
            return back()->with("status", "Страница #{$id} успешно удалена");
        }
    }
}
