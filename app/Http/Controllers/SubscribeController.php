<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Traits\Errors;
use App\Mail\SubscriberMail;
use App\Page;
use App\Post;
use App\Subscriber;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class SubscribeController extends Controller
{
    use Errors;

    public function newSubscriber(Request $request)
    {
        $subscriber = new Subscriber();
        if ($subscriber->isValid($request)) {
            $subscriber->fill($request->all());
            $subscriber->code = Subscriber::getCode();
            $subscriber->save();

            $this->sendMail("mails.subscribe.welcome", $subscriber->email, $subscriber->code);

            return redirect()->route('subscribe.result');
        } else {
            $previousUrl = url()->previous() . "#newsletter";

            return redirect()->to($previousUrl)->withErrors($subscriber->getErrors());
        }
    }

    public function show()
    {
        return view('public.subscribe', $this->getShowParameters());
    }

    public function unsubscribe(Request $request)
    {
        if ($request->has("code") && $request->has('email')) {
            $subscriber = Subscriber::getSubscriber($request->code, $request->email);
            if ($subscriber) {
                if ($subscriber->checkDate()) {
                    $subscriber->delete();

                    return redirect()->route('subscribe.result')->with("status", "Вы успешно отписались от нас. Очень жаль. Мы будем скучать!");
                } else {
                    $this->sendMail("mails.subscribe.unsubscribe", $subscriber->email, $subscriber->code);

                    $this->errors->add("404", "Код устарел. На Вашу почту выслано письмо с новым.");
                    return redirect()->route('subscribe.result')->withErrors($this->getErrors());
                }
            } else {
                $this->errors->add("404", "Пользователь с такой почтой не был подписан на нас или введенный код неверен.");
                return redirect()->route('subscribe.result')->withErrors($this->getErrors());
            }
        } else {
            $this->errors->add("403", "Указанный код или почта неверные.");
            return redirect()->route('subscribe.result')->withErrors($this->getErrors());
        }
    }

    private function getShowParameters()
    {
        return [
            "pages" => Page::getPages(),
            "categories" => Category::getCategories(CATEGORIES_COUNT),
            "posts" => Post::getPosts(POSTS_COUNT_ON_PAGE),
            "recent_posts" => Post::getRecentPosts(RECENT_POSTS_COUNT),
            "tags" => Tag::getTags(TAGS_COUNT)
        ];
    }

    private function sendMail(string $view, $email, string $code)
    {
        Mail::to($email)->send(new SubscriberMail($view, $email, $code));
    }
}
