<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\MessageBag;
use App\Image;

class ImagesController extends Controller
{
    protected $availableExtensions = [
        "png", "jpg", "jpeg", "gif", "svg"
    ];

    private $errors;

    public function __construct()
    {
        $this->errors = new MessageBag;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view("admin.images.index", [
            "images" => Image::orderBy("id", "ASC")->paginate(15)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        if (!count($request->file("images"))) {
            $this->errors->add("404", "Files hasn`t been selected.");
            return back()->withErrors($this->errors);
        }

        foreach ($request->file("images") as $image) {
            if (!in_array($image->extension(), $this->availableExtensions)) {
                $this->errors->add("403", "File {$image->getClientOriginalName()} have unavailable file extension");
                return back()->withErrors($this->errors);
            }

            $file = new Image;
            $file->header = $image->getClientOriginalName();
            $file->alt = $image->getClientOriginalName();

            $validation = $file->isValid(
                [
                    "header" => $file->header,
                    "alt" => $file->alt
                ]
            );
            if ($validation->fails()) {
                return back()->withErrors($validation->errors()->all());
            } else {
                try {
                    $path = Storage::disk('public')->putFileAs("images", $image, $image->getClientOriginalName());
                    $file->filepath = $path;
                    $file->save();
                } catch (\Exception $e) {
                    $this->errors->add("500", "File {$image->getClientOriginalName()} couldn`t been saved. Try again...");
                    return back()->withErrors($this->errors);
                }
            }
        }

        return back()->with("status", "File has been uploaded");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if (!isset($request->id)) {
            $this->errors->add("419", "Image id hasn`t been founded");
            return back()->withErrors($this->errors);
        }

        $image = Image::find($request->id);

        if (!$image) {
            $this->errors->add("404", "Image hasn`t been founded");
            return back()->withErrors($this->errors);
        }

        $validation = $image->isValid(["header" => $request->header, "alt" => $request->alt], $request->id);
        if ($validation->fails()) {
            return back()->withErrors($validation->errors()->all());
        }

        $image->fill($request->all());
        $image->save();

        return back()->with("status", "Image {$image->header} has been updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $image = Image::find($id);

        if (!$image) {
            $this->errors->add("404", "Image hasn`t been founded");
            return back()->withErrors($this->errors);
        }

        try {
            Storage::disk('public')->delete($image->filepath);
            $image->delete();

            return back()->with("status", "Image #{$id} has been deleted");
        } catch (\Exception $e) {
            $this->errors->add("404", "Image hasn`t been deleted. Try again...");
            return back()->withErrors($this->errors);
        }
    }
}
