<?php

namespace App\Http\Controllers;

use App\Http\Traits\Errors;
use App\Http\Traits\RequestHelper;
use App\Page;
use App\Template;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\MessageBag;
use Carbon\Carbon;
use App\Post;
use App\Category;
use App\Image;
use App\Tag;

class PostsController extends Controller
{
    use Errors;
    use RequestHelper;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view("admin.posts.index", [
            "posts" => Post::orderBy("id", "ASC")->paginate(15)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view("admin.posts.create", [
            "categories" => Category::orderBy("id", "ASC")->get(),
            "tags" => Tag::orderBy("id", "ASC")->get(),
            "images" => Image::orderBy("id", "ASC")->get(),
            "templates" => Template::get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $post = new Post;

        $validation = $post->isValid($request);
        if (!$validation->fails()) {
            $post->fill($request->all());
            $post->updateSettings($request->all());
            $this->updatePublish($request, $post);
            $this->updateCategory($request, $post);

            try {
                $this->updateDate($request, $post);
            } catch (\Exception $e) {
                $this->errors->add("500", "Unpublish/publish date or time is incorrect");
                return back()->withInput()->withErrors($this->errors);
            }

            $validatedImages = Image::validateImages($request);
            $validatedTags = Tag::validateTags($request);

            if (!$template = Template::find($request->template_id)) {
                $this->errors->add('404', "Template #{$request->template_id} hasn`t been found");
                return back()->withInput()->withErrors($this->errors);
            }

            $post->template()->associate($template);
            $post->save();
            $post->images()->saveMany($validatedImages);
            $post->tags()->saveMany($validatedTags);

            return redirect()->route("posts.index")->with("status", "Post #{$post->id} has been created");
        } else {
            return back()->withInput()->withErrors($validation->errors()->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $slug
     * @return Response
     */
    public function show($slug)
    {
        $post = Post::where("slug", "=", $slug)->first();

        if (!$post) {
            return abort(404);
        }
        $post->parseSettings();

        try {
            return view($this->getView($post), [
                "post" => $post,
                "pages" => Page::getPages(),
                "categories" => Category::getCategories($post->settings->categories_count),
                "recentPosts" => Post::getRecentPosts($post->settings->recent_posts_count),
                "tags" => Tag::getTags($post->settings->tags_count)
            ]);
        } catch (\Exception $e) {
            return abort(404);
        }
    }

    private function getView($post)
    {
        return 'public.' . $post->template()->first()->filename;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        if (!$post) {
            $this->errors->add("404", "Post #{$id} hasn`t been found");
            return back()->withErrors($this->errors);
        } else {
            $post->parseSettings();
            return view("admin.posts.edit", [
                "post" => $post,
                "categories" => Category::orderBy("id", "ASC")->get(),
                "tags" => Tag::orderBy("id", "ASC")->get(),
                "images" => Image::orderBy("id", "ASC")->get(),
                "templates" => Template::get()
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        if (!$post) {
            $this->errors->add("404", "Post hasn`t been found");
            return back()->withInput()->withErrors($this->errors);
        }

        $validation = $post->isValid($request, $id);
        if (!$validation->fails()) {
            $post->fill($request->all());
            $post->updateSettings($request->all());

            if ($request->is_published == "on") {
                $post->is_published = 1;
            } else {
                $post->is_published = 0;
            }

            if ($post->category_id == "null") {
                $post->category_id = null;
            }

            try {
                if (!isset($request->publish_date) && !isset($request->publish_time)) {
                    $post->publish_time = null;
                } else {
                    $post->publish_time = new Carbon($request->publish_date . " " . $request->publish_time);
                }

                if (!isset($request->unpublish_date) && !isset($request->unpublish_time)) {
                    $post->unpublish_time = null;
                } else {
                    $post->unpublish_time = new Carbon($request->unpublish_date . " " . $request->unpublish_time);
                }
            } catch (\Exception $e) {
                $this->errors->add("500", "Unpublish/publish date or time is incorrect");
                return back()->withInput()->withErrors($this->errors);
            }

            $validatedImages = [];
            if (isset($request->images)) {
                foreach ($request->images as $imageId) {
                    if ($image = Image::find($imageId)) {
                        $validatedImages[] = $image;
                    }
                }
            }

            $validatedTags = [];
            if (isset($request->tags)) {
                foreach ($request->tags as $tagId) {
                    if ($tag = Tag::find($tagId)) {
                        $validatedTags[] = $tag;
                    }
                }
            }

            if (!$template = Template::find($request->template_id)) {
                $this->errors->add('404', "Template #{$request->template_id} hasn`t been found");
                return back()->withInput()->withErrors($this->errors);
            }

            $post->template()->associate($template);
            $post->save();
            $post->images()->detach();
            $post->images()->saveMany($validatedImages);

            $post->tags()->detach();
            $post->tags()->saveMany($validatedTags);

            return redirect()->route("posts.index")->with("status", "Post #{$post->id} has been updated");
        } else {
            return back()->withInput()->withErrors($validation->errors()->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        if (!$post) {
            $this->errors->add("404", "Post #$id hasn`t been found");
            return back()->withErrors($this->errors);
        } else {
            $post->delete();
            return back()->with("status", "Post #{$id} was deleted");
        }
    }
}
