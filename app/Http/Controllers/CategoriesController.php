<?php

namespace App\Http\Controllers;

use App\Http\Traits\Errors;
use App\Http\Traits\RequestHelper;
use App\Http\Traits\Views;
use App\Page;
use App\Post;
use App\Tag;
use App\Template;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\MessageBag;
use Carbon\Carbon;
use App\Category;
use App\Image;

class CategoriesController extends Controller
{
    use Errors;
    use RequestHelper;
    use Views;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view("admin.categories.index", [
            "categories" => Category::where("type", "!=", "system")->orderBy("id", "ASC")->paginate(15)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view("admin.categories.create", [
            "parents" => Category::get(),
            "images" => Image::get(),
            "templates" => Template::get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $category = new Category;

        $validation = $category->isValid($request);
        if (!$validation->fails()) {
            $category->fill($request->all());
            $category->updateSettings($request->all());
            $this->updatePublish($request, $category);
            $this->updateParent($request, $category);

            try {
                $this->updateDate($request, $category);
            } catch (\Exception $e) {
                $this->errors->add("500", "Unpublish/publish date or time is incorrect");
                return back()->withInput()->withErrors($this->errors);
            }

            $validatedImages = Image::validateImages($request);

            if (!$template = Template::find($request->template_id)) {
                $this->errors->add('404', "Template #{$request->template_id} hasn`t been found");
                return back()->withInput()->withErrors($this->errors);
            }

            $category->template()->associate($template);
            $category->save();
            $category->images()->saveMany($validatedImages);

            return redirect()->route("categories.index")->with("status", "Category #{$category->id} has been created");
        } else {
            return back()->withInput()->withErrors($validation->errors()->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $slug
     * @return Response
     */
    public function show($slug)
    {
        $category = Category::where("slug", "=", $slug)->first();

        if (!$category) {
            return abort(404);
        }
        $category->parseSettings();

        try {
            return view($this->getView($category), $this->getShowParameters($category));
        } catch (\Exception $e) {
            return abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $category = Category::find($id);

        if (!$category) {
            $this->errors->add("404", "Category #{$id} hasn`t been found");
            return back()->withErrors($this->errors);
        } else {
            $category->parseSettings();

            return view("admin.categories.edit", [
                "category" => $category,
                "parents" => Category::whereRaw("(parent_id != {$category->id} OR parent_id IS NULL)")->where("id", '!=', $id)->get(),
                "images" => Image::get(),
                "templates" => Template::get()
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        if (!$category) {
            $this->errors->add("404", "Tag $id hasn`t been found");
            return back()->withErrors($this->errors);
        }
        $validation = $category->isValid($request, $id);
        if (!$validation->fails()) {
            $category->fill($request->all());
            $category->updateSettings($request->all());
            $this->updatePublish($request, $category);
            $this->updateParent($request, $category);

            try {
                $this->updateDate($request, $category);
            } catch (\Exception $e) {
                $this->errors->add("500", "Unpublish/publish date or time is incorrect");
                return back()->withInput()->withErrors($this->errors);
            }

            $validatedImages = Image::validateImages($request);

            if (!$template = Template::find($request->template_id)) {
                $this->errors->add('404', "Template #{$request->template_id} hasn`t been found");
                return back()->withInput()->withErrors($this->errors);
            }

            $category->template()->associate($template);
            $category->save();
            $category->images()->detach();
            $category->images()->saveMany($validatedImages);


            return redirect()->route("categories.index")->with("status", "Category #{$category->id} has been updated");
        } else {
            return back()->withInput()->withErrors($validation->errors()->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        if (!$category) {
            $this->errors->add("404", "Category #$id hasn`t been found");
            return back()->withErrors($this->errors);
        } else if ($category->type === "system") {
            $this->errors->add("403", "Category #$id is system and cannot be deleted");
            return back()->withErrors($this->errors);
        } else {
            $category->delete();
            return back()->with("status", "Category #{$id} was deleted");
        }
    }

    private function getShowParameters($category)
    {
        return [
            "category" => $category,
            "pages" => Page::getPages(),
            "subcategories" => $category->children()->take($category->settings->subcategories_count)->get(),
            "posts" => $category->posts()->paginate($category->settings->posts_count_on_page),
            "recentPosts" => Post::getRecentPosts($category->settings->recent_posts_count),
            "tags" => Tag::getTags($category->settings->tags_count)
        ];
    }
}
