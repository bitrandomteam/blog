<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Traits\Errors;
use App\Http\Traits\Views;
use App\Page;
use App\Post;
use App\Tag;
use App\Template;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\MessageBag;
use Carbon\Carbon;

class TagsController extends Controller
{
    use Errors;
    use Views;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        return view("admin.tags.index", [
            "tags" => Tag::orderBy("id", "ASC")->paginate(15)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        return view("admin.tags.create", [
            "templates" => Template::get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $tag = new Tag;

        $validation = $tag->isValid($request);
        if (!$validation->fails()) {
            $tag->fill($request->all());
            $tag->updateSettings($request->all());

            if (!$template = Template::find($request->template_id)) {
                $this->errors->add('404', "Template #{$request->template_id} hasn`t been found");
                return back()->withInput()->withErrors($this->errors);
            }

            $tag->template()->associate($template);
            $tag->save();

            return redirect()->route("tags.index")->with("status", "Tag #{$tag->id} has been created");
        } else {
            return back()->withInput()->withErrors($validation->errors()->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $slug
     * @return Response
     */
    public function show(Request $request, $slug)
    {
        $tag = Tag::where("slug", "=", $slug)->first();

        if (!$tag) {
            return abort(404);
        }
        $tag->parseSettings();

        try {
            return view($this->getView($tag), [
                "tag" => $tag,
                "pages" => Page::getPages(),
                "categories" => Category::getCategories($tag->settings->categories_count),
                "posts" => $tag->postsMany()->paginate($tag->settings->posts_count_on_page),
                "recentPosts" => Post::getRecentPosts($tag->settings->recent_posts_count),
                "tags" => Tag::getTags($tag->settings->tags_count)
            ]);
        } catch (\Exception $e) {
            return abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $tag = Tag::find($id);

        if (!$tag) {
            $this->errors->add("404", "Tag #{$id} hasn`t been found");
            return back()->withErrors($this->errors);
        } else {
            return view("admin.tags.edit", [
                "tag" => $tag,
                "templates" => Template::get()
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $tag = Tag::find($id);

        if (!$tag) {
            $this->errors->add("404", "Tag $id hasn`t been found");
            return back()->withErrors($this->errors);
        }

        $validation = $tag->isValid($request, $id);
        if (!$validation->fails()) {
            $tag->fill($request->all());
            $tag->updateSettings($request->all());

            if (!$template = Template::find($request->template_id)) {
                $this->errors->add('404', "Template #{$request->template_id} hasn`t been found");
                return back()->withInput()->withErrors($this->errors);
            }

            $tag->template()->associate($template);
            $tag->save();

            return redirect()->route("tags.index")->with("status", "Tag #{$tag->id} has been updated");
        } else {
            return back()->withInput()->withErrors($validation->errors()->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $tag = Tag::find($id);

        if (!$tag) {
            $this->errors->add("404", "Tag #$id hasn`t been found");
            return back()->withErrors($this->errors);
        } else {
            $tag->delete();
            return back()->with("status", "Tag #{$id} was deleted");
        }
    }
}
