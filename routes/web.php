<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('admin')->group(function () {
    Auth::routes(['register' => true]);

    Route::get('/', 'PostsController@index')->middleware("auth");

    Route::resource('tags', 'TagsController')->except(['show'])->middleware("auth");
    Route::resource('categories', 'CategoriesController')->except(['show'])->middleware("auth");
    Route::resource('images', 'ImagesController')->except(['show'])->middleware("auth");
    Route::resource('posts', 'PostsController')->except(['show'])->middleware("auth");
    Route::resource('pages', 'PagesController')->except(['show'])->middleware("auth");
});

Route::get('/subscribe', "SubscribeController@show")->name("subscribe.result");
Route::post('/subscribe', "SubscribeController@newSubscriber")->name("subscribe.new");
Route::get('/unsubscribe', "SubscribeController@unsubscribe")->name("subscribe.unsubscribe");

Route::get("/post/{slug?}", "PostsController@show")->name("posts.show");
Route::get("/category/{slug?}", "CategoriesController@show")->name("categories.show");
Route::get("/tag/{slug?}", "TagsController@show")->name("tags.show");
Route::get('/{slug?}', "PagesController@show")->name("pages.show");
