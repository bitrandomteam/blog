@extends("layouts.public")

@push('seo_title'){{ $post->seo_title }}@endpush
@push('seo_description'){{ $post->seo_description }}@endpush
@push('seo_keywords'){{ $post->seo_keywords }}@endpush
@push('styles')
    <link rel="stylesheet" href="{{ asset('/css/public/posts.css') }}">
@endpush

@section('content')
    <div class="post">
        <div class="row">
            <div class="col-md-12 p-0 mt-5">
                <div class="post__image" style="background-image: url({{ ($post->images()->first()) ? asset("/storage/" . $post->images()->first()->filepath) : "/storage/images/no-photo.png" }})">
                    <h1 class="post__header">{{ $post->header }}</h1>
                </div>
            </div>
        </div>
        <div class="container-fluid mt-5">
            <div class="row justify-content-center">
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="post__category d-flex justify-content-between">
                                <a class="orange" href="{{ route("categories.show", $post->category()->first()->slug) }}">Категория: {{ $post->category()->first()->header }}</a>
                                <span class="orange">{{ ($post->publish_date !== null) ? \Carbon\Carbon::parse($post->publish_date)->format("d M, Y") : \Carbon\Carbon::parse($post->created_at)->format("d M, Y") }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="post__text">
                                {!! $post->text !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    @include('sublayouts.components.subcategories', ['subcategories' => $categories, "text_categories" => "Категории"])
                    @include('sublayouts.components.recentPosts')
                    @include('sublayouts.components.tags')
                    @include('sublayouts.components.newsletter')
                </div>
            </div>
        </div>
    </div>
@endsection
