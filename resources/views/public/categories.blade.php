@extends("layouts.public")

@push('seo_title'){{ $page->seo_title }}@endpush
@push('seo_description'){{ $page->seo_description }}@endpush
@push('seo_keywords'){{ $page->seo_keywords }}@endpush
@push('styles')
    <link rel="stylesheet" href="{{ asset('/css/public/categories.css') }}">
@endpush

@section("content")
    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-12 m-3 ml-5 text-left">
                <h1 class="page__header">{{ $page->header }}</h1>
            </div>
        </div>
    </div>
    <div class="categories">
        <div class="container-fluid">
            <div class="row justify-content-center">
                @each('sublayouts.components.category', $categories, 'category', 'sublayouts.components.empty')
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 mt-5 mb-5">
                <div class="page__description">
                    {!! $page->text !!}
                </div>
            </div>
        </div>
    </div>
@endsection
