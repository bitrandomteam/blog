@extends("layouts.public")

@push('seo_title'){{ $tag->seo_title }}@endpush
@push('seo_description'){{ $tag->seo_description }}@endpush
@push('seo_keywords'){{ $tag->seo_keywords }}@endpush
@push('styles')
    <link rel="stylesheet" href="{{ asset('/css/public/categories.css') }}">
@endpush

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-6 p-0 mt-5">
            <h1 class="category__header">{{ $tag->header }}</h1>
        </div>
    </div>
    <div class="container-fluid mt-5">
        <div class="row justify-content-center">
            <div class="col-md-5">
                @include('sublayouts.components.posts')
                @include('sublayouts.components.pagination', ['data' => $posts])
            </div>
            <div class="col-md-4">
                @include('sublayouts.components.subcategories', ['subcategories' => $categories, "text_categories" => "Категории"])
                @include('sublayouts.components.recentPosts')
                @include('sublayouts.components.tags')
                @include('sublayouts.components.newsletter')
            </div>
        </div>
    </div>
@endsection
