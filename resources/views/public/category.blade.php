@extends("layouts.public")

@push('seo_title'){{ $category->seo_title }}@endpush
@push('seo_description'){{ $category->seo_description }}@endpush
@push('seo_keywords'){{ $category->seo_keywords }}@endpush
@push('styles')
    <link rel="stylesheet" href="{{ asset('/css/public/categories.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12 p-0 mt-5">
            <div class="category" style="background-image: url({{ ($category->images()->first()) ? asset("/storage/" . $category->images()->first()->filepath) : "/storage/images/no-photo.png" }})">
                <h1 class="category__header">{{ $category->header }}</h1>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-5">
        <div class="row justify-content-center">
            <div class="col-md-5">
                @include('sublayouts.components.posts')
                @include('sublayouts.components.pagination', ["data" => $posts ])
            </div>
            <div class="col-md-4">
                @include('sublayouts.components.subcategories')
                @include('sublayouts.components.recentPosts')
                @include('sublayouts.components.tags')
                @include('sublayouts.components.newsletter')
            </div>
        </div>
    </div>
@endsection
