@extends("layouts.public")

@push('seo_title'){{ $page->seo_title }}@endpush
@push('seo_description'){{ $page->seo_description }}@endpush
@push('seo_keywords'){{ $page->seo_keywords }}@endpush

@push('styles')
    <link rel="stylesheet" href="{{ asset('/css/public/page.css') }}">
@endpush

@section("content")
    <div class="page">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center mt-3 mb-5">
                    <div class="page__header">
                        <h1 class="orange ttu">{{ $page->header }}</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="page__image">
                        @if ($page->images()->first())
                        <img src="{{ asset("/storage/" . $page->images()->first()->filepath) }}" alt="{{ $page->images()->first()->alt }}">
                        @else
                        <img src="/storage/images/no-photo.png" alt="No photo">
                        @endif
                    </div>
                </div>
                <div class="offset-1 col-md-6">
                    <div class="page__text">
                        {!! $page->text !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
