@extends("layouts.public")

@push('seo_title'){{ $page->seo_title }}@endpush
@push('seo_description'){{ $page->seo_description }}@endpush
@push('seo_keywords'){{ $page->seo_keywords }}@endpush

@push('styles')
    <link rel="stylesheet" href="{{ asset('/css/public/home.css') }}">
@endpush

@section("content")
    <div class="welcome__text text-center">
        {!! $page->text !!}
    </div>
    <div class="get__started get__started_link text-center mt-5">
        <a href="{{ route("pages.show", "categories") }}">Начнем</a>
    </div>
@endsection
