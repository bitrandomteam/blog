@extends("layouts.public")

@push('seo_title')Подписка@endpush
@push('seo_description')@endpush
@push('seo_keywords')@endpush
@push('styles')
    <link rel="stylesheet" href="{{ asset('/css/public/newsletter.css') }}">
@endpush

@section("content")
    @if (session('status'))
        {{ session('status') }}
    @elseif ($errors->all())
        @foreach($errors->all() as $error)
            <div class="error">
                {{ $error }}
            </div>
        @endforeach
    @else
        <h1>Добро пожаловать!</h1>
        <p>Подписка успешно оформлена и теперь Вы всегда будете в курсе событий!</p>
    @endif
@endsection
