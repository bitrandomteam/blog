<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!doctype html>
<html lang="en">

<head>
    <title>@stack('title')</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- Material Kit CSS -->
    <link href="/css/admin/material-dashboard.min.css" rel="stylesheet" />
    <link href="/css/admin/admin.css" rel="stylesheet" />

    @stack('scripts')

    <script src="https://cdn.tiny.cloud/1/j358wes8sc5yld5osrfm1ntzsinsg54alalp87ou9z3r674v/tinymce/5/tinymce.min.js"></script>
    <script>tinymce.init({ selector: ".mce" });</script>
</head>

<body>
    <div class="wrapper ">
      <div class="sidebar" data-color="azure" data-background-color="white" data-image="/img/sidebar-1.jpg">
        <div class="logo">
          <a href="/" class="simple-text logo-normal">
            Сайт
          </a>
        </div>
        <div class="sidebar-wrapper">
          <ul class="nav">
              @php ($url = url()->current())
            <li class="nav-item {{ $url === route("categories.index") || $url === route("categories.create") || $url === route("categories.edit", @$category->id or 1)  ? "active" : "" }}">
              <a class="nav-link" href="{{ route("categories.index") }}">
                <i class="material-icons">Categories</i>
                <p>Категории</p>
              </a>
            </li>
            <li class="nav-item {{ $url === route("tags.index") || $url === route("tags.create") || $url === route("tags.edit", @$tag->id or 1)  ? "active" : "" }}">
              <a class="nav-link" href="{{ route("tags.index") }}">
                <i class="material-icons">Tags</i>
                <p>Теги</p>
              </a>
            </li>
            <li class="nav-item {{ $url === route("posts.index") || $url === route("posts.create") || $url === route("posts.edit", @$post->id or 1)  ? "active" : "" }}">
              <a class="nav-link" href="{{ route("posts.index") }}">
                <i class="material-icons">Posts</i>
                <p>Посты</p>
              </a>
            </li>
            <li class="nav-item {{ $url === route("pages.index") || $url === route("pages.create") || $url === route("pages.edit", @$page->id or 1)  ? "active" : "" }}">
              <a class="nav-link" href="{{ route("pages.index") }}">
                <i class="material-icons">Pages</i>
                <p>Страницы</p>
              </a>
            </li>
            <li class="nav-item {{ $url === route("images.index")  ? "active" : "" }}">
              <a class="nav-link" href="{{ route("images.index") }}">
                <i class="material-icons">Image</i>
                <p>Изображения</p>
              </a>
            </li>
              <li class="nav-item d-flex justify-content-center">
                  <form action="{{ route("logout") }}" method="POST">
                      @csrf
                      <input type="submit" class="btn btn-secondary" value="Выйти">
                    </form>
            </li>
          </ul>
        </div>
      </div>
      <div class="main-panel">
        <!-- End Navbar -->
        @yield("content")
        <footer class="footer">
            @yield("footer")
        </footer>
      </div>
    </div>
    <!--   Core JS Files   -->
    <script src="/js/core/jquery.min.js"></script>
    <script src="/js/core/popper.min.js"></script>
    <script src="/js/core/bootstrap-material-design.min.js"></script>
    <script src="/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <!-- Plugin for the momentJs  -->
    <script src="/js/plugins/moment.min.js"></script>
    <!--  Plugin for Sweet Alert -->
    <script src="/js/plugins/sweetalert2.js"></script>
    <!-- Forms Validations Plugin -->
    <script src="/js/plugins/jquery.validate.min.js"></script>
    <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
    <script src="/js/plugins/jquery.bootstrap-wizard.js"></script>
    <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
    <script src="/js/plugins/bootstrap-selectpicker.js"></script>
    <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
    <script src="/js/plugins/bootstrap-datetimepicker.min.js"></script>
    <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
    <script src="/js/plugins/jquery.dataTables.min.js"></script>
    <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
    <script src="/js/plugins/bootstrap-tagsinput.js"></script>
    <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
    <script src="/js/plugins/jasny-bootstrap.min.js"></script>
    <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
    <script src="/js/plugins/fullcalendar.min.js"></script>
    <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
    <script src="/js/plugins/jquery-jvectormap.js"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="/js/plugins/nouislider.min.js"></script>
    <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <!-- Library for adding dinamically elements -->
    <script src="/js/plugins/arrive.min.js"></script>
    <!--  Google Maps Plugin    -->
    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
    <!-- Chartist JS -->
    <script src="/js/plugins/chartist.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="/js/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="/js/material-dashboard.js?v=2.1.1" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        md.initDashboardPageCharts();
      });
    </script>
</body>

</html>
