<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@stack('seo_title')</title>
    <meta name="description" content="@stack('seo_description')">
    <meta name="keywords" content="@stack('seo_keywords')">

    @include('sublayouts.libs.jquery')
    @include('sublayouts.libs.popper')
    @include('sublayouts.libs.bootstrap')

    <!--Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <link href="{{ asset('/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <!--Styles-->
    <link rel="stylesheet" href="{{ asset('/css/public/main.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/public/menu.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/public/search.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/public/footer.css') }}">

    @stack('styles')

    @stack('scripts')
</head>
<body>
    <nav>
        <div class="container">
            <div class="row">
                <div class="col-md-12 d-flex justify-content-center">
                    @include('sublayouts.menu.top')
                    @include('sublayouts.menu.mobile')
                </div>
            </div>
        </div>
    </nav>
    <main>
        <div class="container-fluid mt-5 p-0">
            <div class="row">
                <div class="col-md-12 text-center">
                    <a href="/">
                    @include('sublayouts.components.logo')
                    </a>
                </div>
            </div>
        </div>
    @yield('content')
    </main>
    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 text-center">
                    @include('sublayouts.components.footerLogo')
                </div>
            </div>
            <div class="row justify-content-center mt-5">
                <div class="col-md-5">
                    @include('sublayouts.components.footerText')
                </div>
            </div>
            <div class="row justify-content-center mt-3">
                <div class="col-md-5">
                    @include('sublayouts.components.footerCopyright')
                </div>
            </div>
        </div>
    </footer>

    @include('sublayouts.dev.livereload')
</body>
</html>
