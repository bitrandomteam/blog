@extends("layouts.admin")

@push("scripts")
<!-- jQuery 1.8 or later, 33 KB -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Fotorama from CDNJS, 19 KB -->
<link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
@endpush

@section("content")
<div class="content">
@foreach ($errors->all() as $error)
<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <i class="material-icons">close</i>
    </button>
    <span>
      <b> Внимание - </b> {{ $error }}</span>
  </div>
@endforeach
@if (session('status'))
    <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <i class="material-icons">close</i>
    </button>
    <span>
      <b> Выполнено - </b> {{session('status')}} </span>
  </div>
@endif
<!-- Add images to <div class="fotorama"></div> -->
<div
    class="fotorama"
    data-click="false"
    data-nav="thumbs"
    data-width="100%"
    data-height="70%"
>
@foreach ($images as $image)
<div
    style="width: 100%; height: 100%"
    data-img='/storage/{{ $image->filepath }}'
    data-caption="{{ $image->alt }}"
>
    <form action="{{ route("images.destroy", ["id" => $image->id]) }}" method="POST">
        @csrf
        @method("DELETE")
        <input type="submit" class="btn btn-danger" value="X">
    </form>
    <input
        type="hidden"
        name="id"
        value="{{ $image->id }}"
    >
</div>
@endforeach
</div>

<div class="modal" tabindex="-1" role="dialog" id="changeCaption">
    <form action="{{ route("images.update", ["id" => 0]) }}" method="POST">
        @csrf
        @method("PUT")
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Обновление изображения</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div class="row">
                  <div class="col-md-12">
                      <input id="input__caption" type="text" name="header" class="form-control" placeholder="Введите название изображения...">
                  </div>
                  <div class="col-md-12 mt-3">
                      <input id="input__caption" type="text" name="alt" class="form-control" placeholder="Введите альтернативный текст изображения...">
                  </div>
              </div>
            <input type="hidden" name="id">
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Сохранить</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
          </div>
        </div>
      </div>
  </form>
</div>

<form action="{{ route("images.store") }}" method="POST" enctype='multipart/form-data'>
    @csrf
    <label class="btn btn-secondary">
        <input class="d-none" type="file" name="images[]" multiple>
        <span>Обзор файлов...</span>
    </label>
    <input class="btn btn-primary" type="submit" value="загрузить">
</form>
    @include('sublayouts.components.pagination', ["data" => $images])
</div>

<script>
    jQuery(document).ready(function() {
        $(".fotorama__caption").dblclick(function() {
            let id = $(this).parents(".fotorama__stage__frame").find("[name=id]").val();

            $("#changeCaption [name=id]").val(id);
            $('#changeCaption').modal('toggle');
        });
    });
</script>
@endsection
