@extends("layouts.admin")

@push('title')
    Список категорий
@endpush

@section("content")
<div class="content">
    @foreach ($errors->all() as $error)
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span>
          <b> Внимание - </b> {{ $error }}</span>
      </div>
    @endforeach
    @if (session('status'))
        <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">закрыть</i>
        </button>
        <span>
          <b> Выполнено - </b> {{session('status')}} </span>
      </div>
    @endif
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-plain">
            <div class="card-header card-header-primary">
              <h4 class="card-title mt-0">Категории</h4>
            </div>
          <div class="card-body">
              <div class="d-flex justify-content-end">
                  <a href="{{ route("categories.create") }}" class="btn btn-success">Добавить</a>
              </div>
            <div class="table-responsive">
              <table class="table table-hover">
                <thead class="">
                  <th>
                    ID
                  </th>
                  <th>
                    Заголовок
                  </th>
                  <th>
                    Алиас
                  </th>
                  <th>
                    Родительская категория
                  </th>
                  <th>
                    Seo-заголовок
                  </th>
                  <th>
                    Ключевые слова
                  </th>
                  <th>
                    Seo-описание
                  </th>
                  <th>
                    Опубликовано
                  </th>
                  <th>
                    Дата публикации
                  </th>
                  <th>
                    Дата снятия с публикации
                  </th>
                  <th>Действия</th>
                </thead>
                <tbody>
                    @foreach($categories as $category)
                    <tr>
                      <td>
                        {{ $category->id }}
                      </td>
                      <td>
                          <a href="{{ route("categories.show", ["parentSlug" => @$category->parent()->first()->slug, "slug" => $category->slug]) }}">{{ $category->header }}</a>
                      </td>
                      <td>
                        {{ $category->slug }}
                      </td>
                      <td>
                          @if ($category->parent()->first() && $category->parent()->first()->type !== "system")
                        <a href="{{ route("categories.edit", ["id" => $category->parent()->first()->id])}}">{{ $category->parent()->first()->header }}</a>
                          @else
                        Отсутствует
                          @endif
                      </td>
                      <td>
                        {{ $category->seo_title }}
                      </td>
                      <td>
                        {{ $category->seo_keywords }}
                      </td>
                      <td>
                        {{ $category->seo_description }}
                      </td>
                      <td>
                        {{ $category->is_published ? "Опубликовано" : "Не опубликовано" }}
                      </td>
                      <td>
                        {{ $category->publish_time ? \Carbon\Carbon::parse($category->publish_time)->format("h:i:s d-m-y") : "Не важно" }}
                      </td>
                      <td>
                        {{ $category->unpublish_time ? \Carbon\Carbon::parse($category->unpublish_time)->format("h:i:s d-m-y") : "Не важно" }}
                      </td>
                      <td class="td-actions text-right d-flex">
                        <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Редактировать" aria-describedby="tooltip803439">
                          <a href="{{ route("categories.edit", ["id" => $category->id]) }}"><i class="material-icons">edit</i></a>
                        </button>
                        <form action="{{ route("categories.destroy", ["id" => $category->id]) }}" method="POST" onsubmit="return confirm('Вы действительно хотите удалить категорию #{{ $category->id }}?')">
                            @csrf
                            @method("DELETE")
                            <button type="submit" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Удалить">
                              <i class="material-icons">close</i>
                            </button>
                        </form>
                      </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
              @if (!count($categories))
              <div class="alert alert-info" role="alert">
                Категории не были найдены
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    @include('sublayouts.components.pagination', ["data" => $categories])
</div>
@endsection
