@extends("layouts.admin")

@push('title')
    Список тегов
@endpush

@section("content")
<div class="content">
    @foreach ($errors->all() as $error)
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span>
          <b> Внимание - </b> {{ $error }}</span>
      </div>
    @endforeach
    @if (session('status'))
        <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span>
          <b> Выполнено - </b> {{session('status')}} </span>
      </div>
    @endif
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-plain">
            <div class="card-header card-header-primary">
              <h4 class="card-title mt-0">Теги</h4>
            </div>
          <div class="card-body">
              <div class="d-flex justify-content-end">
                  <a href="{{ route("tags.create") }}" class="btn btn-success">Добавить</a>
              </div>
            <div class="table-responsive">
              <table class="table table-hover">
                <thead class="">
                  <th>
                    ID
                  </th>
                  <th>
                    Заголовок
                  </th>
                  <th>
                    Алиас
                  </th>
                  <th>
                    Seo-заголовок
                  </th>
                  <th>
                    Ключевые слова
                  </th>
                  <th>
                    Seo-описание
                  </th>
                  <th>Действия</th>
                </thead>
                <tbody>
                    @foreach($tags as $tag)
                    <tr>
                      <td>
                        {{ $tag->id }}
                      </td>
                      <td>
                          <a href="{{ route("tags.show", $tag->slug) }}">{{ $tag->header }}</a>
                      </td>
                      <td>
                        {{ $tag->slug }}
                      </td>
                      <td>
                        {{ $tag->seo_title }}
                      </td>
                      <td>
                        {{ $tag->seo_keywords }}
                      </td>
                      <td>
                        {{ $tag->seo_description }}
                      </td>
                      <td class="td-actions text-right d-flex">
                        <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Редактировать" aria-describedby="tooltip803439">
                          <a href="{{ route("tags.edit", ["id" => $tag->id]) }}"><i class="material-icons">edit</i></a>
                        </button>
                        <form action="{{ route("tags.destroy", ["id" => $tag->id]) }}" method="POST" onsubmit="return confirm('Вы действительно хотите удалить тег #{{ $tag->id }}')">
                            @csrf
                            @method("DELETE")
                            <button type="submit" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Удалить">
                              <i class="material-icons">close</i>
                            </button>
                        </form>
                      </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
              @if (!count($tags))
              <div class="alert alert-info" role="alert">
                Теги не найдены
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    @include('sublayouts.components.pagination', ["data" => $tags])
</div>
@endsection
