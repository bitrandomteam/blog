@extends("layouts.admin")

@push('title')
    Создание тега
@endpush

@section("content")
<div class="content">
    @foreach ($errors->all() as $error)
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span>
          <b> Внимание - </b> {{ $error }}</span>
      </div>
    @endforeach
    @if (session('status'))
        <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span>
          <b> Выполнено - </b> {{session('status')}} </span>
      </div>
    @endif
    <form action="{{ route("tags.store") }}" method="POST">
        @csrf
      <div class="container-fluid">
        <div class="row justify-content-center">
          <div class="col-md-10">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Создание тега</h4>
              </div>
              <div class="card-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Заголовок</label>
                          <input type="text" class="form-control" name="header" value="{{ old("header") }}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Алиас</label>
                          <input type="text" class="form-control" name="slug" value="{{ old("slug") }}">
                        </div>
                      </div>
                    </div>
                    @if ($templates->count())
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="template_id" class="bmd-label-floating">Шаблон</label>
                                    <select id="template_id" name="template_id" class="form-control">
                                        @foreach($templates as $template)
                                            <option value="{{ $template->id }}" {{ ($template->id == old("template_id") ? "selected" : "") }}>{{ $template->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Seo-заголовок</label>
                          <input type="text" class="form-control" name="seo_title"  value="{{ old("seo_title") }}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Ключевые слова</label>
                          <input type="text" class="form-control" name="seo_keywords"  value="{{ old("seo_keywords") }}">
                        </div>
                      </div>
                    </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Seo-описание</label>
                        <div class="form-group">
                          <textarea class="form-control" rows="5" name="seo_description" value="{{ old("seo_description") }}">{{ old("seo_description") }}</textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Описание</label>
                        <div class="form-group">
                          <textarea class="form-control mce" rows="20" name="description" value="{{ old("description") }}">{{ old("description") }}</textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary pull-right">Создать</button>
                  <a href="{{ route("tags.index") }}" class="btn btn-success" onclick="return confirm('Вы уверены?')">Вернуться</a>
                  <div class="clearfix"></div>
              </div>
            </div>
              <div class="collapse show" id="collapseExample">
                  <div class="card card-body">
                      <div class="form-group">
                          <label class="bmd-label-floating">Количество постов на странице</label>
                          <input
                              type="number"
                              class="form-control"
                              name="posts_count_on_page"
                              value="{{ old("posts_count_on_page") ? old("posts_count_on_page") : POSTS_COUNT_ON_PAGE }}">
                      </div>
                      <div class="form-group">
                          <label class="bmd-label-floating">Recent posts count</label>
                          <input
                              type="number"
                              class="form-control"
                              name="recent_posts_count"
                              value="{{ old("recent_posts_count") ? old("recent_posts_count") : RECENT_POSTS_COUNT }}">
                      </div>
                      <div class="form-group">
                          <label class="bmd-label-floating">Количество избранных постов на странице</label>
                          <input
                              type="number"
                              class="form-control"
                              name="featured_posts_count"
                              value="{{ old("featured_posts_count") ? old("featured_posts_count") : FEATURED_POSTS_COUNT }}">
                      </div>
                      <div class="form-group">
                          <label class="bmd-label-floating">Количество тегов</label>
                          <input
                              type="number"
                              class="form-control"
                              name="tags_count"
                              value="{{ old("tags_count") ? old("tags_count") : TAGS_COUNT }}">
                      </div>
                      <div class="form-group">
                          <label class="bmd-label-floating">Количество категорий</label>
                          <input
                              type="number"
                              class="form-control"
                              name="categories_count"
                              value="{{ old("categories_count") ? old("categories_count") : CATEGORIES_COUNT }}">
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
  </form>
</div>
@endsection
