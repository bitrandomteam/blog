@extends("layouts.admin")

@push('title')
    Список страниц
@endpush

@section("content")
<div class="content">
    @foreach ($errors->all() as $error)
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span>
          <b> Внимание - </b> {{ $error }}</span>
      </div>
    @endforeach
    @if (session('status'))
        <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span>
          <b> Выполнено - </b> {{session('status')}} </span>
      </div>
    @endif
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-plain">
            <div class="card-header card-header-primary">
              <h4 class="card-title mt-0">Страницы</h4>
            </div>
          <div class="card-body">
              <div class="d-flex justify-content-end">
                  <a href="{{ route("pages.create") }}" class="btn btn-success">Добавить</a>
              </div>
            <div class="table-responsive">
              <table class="table table-hover">
                <thead class="">
                  <th>
                    ID
                  </th>
                  <th>
                    Заголовок
                  </th>
                  <th>
                    Алиас
                  </th>
                  <th>
                    Seo-заголовок
                  </th>
                  <th>
                    Ключевые слова
                  </th>
                  <th>
                    Seo-описание
                  </th>
                  <th>
                    Опубликовано
                  </th>
                  <th>
                    Дата публикации
                  </th>
                  <th>
                    Дата снятия с публикации
                  </th>
                  <th>Действия</th>
                </thead>
                <tbody>
                    @foreach($pages as $page)
                    <tr>
                      <td>
                        {{ $page->id }}
                      </td>
                      <td>
                          <a href="{{ route("pages.show", $page->slug) }}">{{ $page->header }}</a>
                      </td>
                      <td>
                        {{ $page->slug }}
                      </td>
                      <td>
                        {{ $page->seo_title }}
                      </td>
                      <td>
                        {{ $page->seo_keywords }}
                      </td>
                      <td>
                        {{ $page->seo_description }}
                      </td>
                      <td>
                        {{ $page->is_published ? "Опубликовано" : "Не опубликовано" }}
                      </td>
                      <td>
                        {{ $page->publish_time ? \Carbon\Carbon::parse($page->publish_time)->format("h:i:s d-m-y") : "Не важно" }}
                      </td>
                      <td>
                        {{ $page->unpublish_time ? \Carbon\Carbon::parse($page->unpublish_time)->format("h:i:s d-m-y") : "Не важно" }}
                      </td>
                      <td class="td-actions text-right d-flex">
                        <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Редактировать" aria-describedby="tooltip803439">
                          <a href="{{ route("pages.edit", ["id" => $page->id]) }}"><i class="material-icons">edit</i></a>
                        </button>
                        <form action="{{ route("pages.destroy", ["id" => $page->id]) }}" method="POST" onsubmit="return confirm('Вы действительно хотите удалить страницу #{{ $page->id }}?')">
                            @csrf
                            @method("DELETE")
                            <button type="submit" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Удалить">
                              <i class="material-icons">close</i>
                            </button>
                        </form>
                      </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
              @if (!count($pages))
              <div class="alert alert-info" role="alert">
                Страницы не были найдены
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    @include('sublayouts.components.pagination', ["data" => $pages])
</div>
@endsection
