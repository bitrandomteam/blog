@extends("layouts.admin")

@push('title')
    Список постов
@endpush

@section("content")
<div class="content">
    @foreach ($errors->all() as $error)
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span>
          <b> Внимание - </b> {{ $error }}</span>
      </div>
    @endforeach
    @if (session('status'))
        <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span>
          <b> Выполнено - </b> {{session('status')}} </span>
      </div>
    @endif
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-plain">
            <div class="card-header card-header-primary">
              <h4 class="card-title mt-0">Посты</h4>
            </div>
          <div class="card-body">
              <div class="d-flex justify-content-end">
                  <a href="{{ route("posts.create") }}" class="btn btn-success">Добавить</a>
              </div>
            <div class="table-responsive">
              <table class="table table-hover">
                <thead class="">
                  <th>
                    ID
                  </th>
                  <th>
                    Заголовок
                  </th>
                  <th>
                    Алиас
                  </th>
                  <th>
                    Категория
                  </th>
                  <th>
                    Seo-заголовок
                  </th>
                  <th>
                    Ключевые слова
                  </th>
                  <th>
                    Seo-описание
                  </th>
                  <th>
                    Опубликовано
                  </th>
                  <th>
                    Дата публикации
                  </th>
                  <th>
                    Дата снятия с публикации
                  </th>
                  <th>Действия</th>
                </thead>
                <tbody>
                    @foreach($posts as $post)
                    <tr>
                      <td>
                        {{ $post->id }}
                      </td>
                      <td>
                          <a href="{{ route("posts.show", $post->slug) }}">{{ $post->header }}</a>
                      </td>
                      <td>
                        {{ $post->slug }}
                      </td>
                      <td>
                          @if ($post->category()->first())
                        <a href="{{ route("categories.edit", ["id" => $post->category()->first()->id])}}">{{ $post->category()->first()->header }}</a>
                          @endif
                      </td>
                      <td>
                        {{ $post->seo_title }}
                      </td>
                      <td>
                        {{ $post->seo_keywords }}
                      </td>
                      <td>
                        {{ $post->seo_description }}
                      </td>
                      <td>
                        {{ $post->is_published ? "Опубликовано" : "Не опубликовано" }}
                      </td>
                      <td>
                        {{ $post->publish_time ? \Carbon\Carbon::parse($post->publish_time)->format("h:i:s d-m-y") : "Не важно" }}
                      </td>
                      <td>
                        {{ $post->unpublish_time ? \Carbon\Carbon::parse($post->unpublish_time)->format("h:i:s d-m-y") : "Не важно" }}
                      </td>
                      <td class="td-actions text-right d-flex">
                        <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Редактировать" aria-describedby="tooltip803439">
                          <a href="{{ route("posts.edit", ["id" => $post->id]) }}"><i class="material-icons">edit</i></a>
                        </button>
                        <form action="{{ route("posts.destroy", ["id" => $post->id]) }}" method="POST" onsubmit="return confirm('Вы действительно хотите удалить пост #{{ $post->id }}')">
                            @csrf
                            @method("DELETE")
                            <button type="submit" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Удалить">
                              <i class="material-icons">close</i>
                            </button>
                        </form>
                      </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
              @if (!count($posts))
              <div class="alert alert-info" role="alert">
                Посты не были найдены
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    @include('sublayouts.components.pagination', ["data" => $posts])
</div>
@endsection
