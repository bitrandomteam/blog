@extends("layouts.admin")

@push('title')
    #{{ $post->id }} {{ $post->header }} - редактирование
@endpush

@section("content")
<div class="content">
    @foreach ($errors->all() as $error)
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span>
          <b> Внимание - </b> {{ $error }}</span>
      </div>
    @endforeach
    @if (session('status'))
        <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span>
          <b> Выполнено - </b> {{session('status')}} </span>
      </div>
    @endif
    <form action="{{ route("posts.update", ["id" => $post->id ]) }}" method="POST">
        @method("PUT")
        @csrf
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Обновление поста #{{ $post->id }}</h4>
              </div>
              <div class="card-body">
                <form>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Заголовок</label>
                          <input type="text" class="form-control" name="header" value="{{ $post->header }}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Алиас</label>
                          <input type="text" class="form-control" name="slug" value="{{ $post->slug }}">
                        </div>
                      </div>
                    </div>
                    @if ($templates->count())
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="template_id" class="bmd-label-floating">Шаблон</label>
                                    <select id="template_id" name="template_id" class="form-control">
                                        @foreach($templates as $template)
                                            <option value="{{ $template->id }}" {{ ($post->template()->first() && $template->id == $post->template()->first()->id ? "selected" : "") }}>{{ $template->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Категория</label>
                          <select name="category_id" class="form-control">
                              @foreach($categories as $category)
                              <option value="{{ $category->id }}" {{ ($category->id == $post->category_id ? "selected" : "") }}>{{ $category->header }}</option>
                              @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Теги</label>
                          <select name="tags[]" class="form-control tags__select" multiple>
                              @foreach($tags as $tag)
                              <option value="{{ $tag->id }}" {{ ($post->tags()->get() && in_array($tag->id, $post->tags()->get()->toArray())) ? "selected" : "" }}>{{ $tag->header }}</option>
                              @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Seo-заголовок</label>
                          <input type="text" class="form-control" name="seo_title"  value="{{ $post->seo_title }}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Ключевые слова</label>
                          <input type="text" class="form-control" name="seo_keywords"  value="{{ $post->seo_keywords }}">
                        </div>
                      </div>
                    </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Seo-описание</label>
                        <div class="form-group">
                          <textarea class="form-control" rows="5" name="seo_description" value="{{ $post->seo_description }}">{{ $post->seo_description }}</textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Описание</label>
                        <div class="form-group">
                          <textarea class="form-control mce" rows="20" name="description" value="{{ $post->description }}">{{ $post->description }}</textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Текст</label>
                        <div class="form-group">
                          <textarea class="form-control mce" rows="20" name="text" value="{{ $post->text }}">{{ $post->text }}</textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary pull-right">Обновить</button>
                  <a href="{{ route("posts.index") }}" class="btn btn-success" onclick="return confirm('Вы уверены?')">Вернуться</a>
                  <div class="clearfix"></div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-profile">
              <div class="card-body">
                  <div class="row">
                      <div class="col-md-12">
                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#postImage">
                              Изменить изображение
                          </button>
                          <!-- Modal -->
                          <div class="modal fade" id="postImage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Выберите изображение для поста</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  @forelse ($images as $image)
                                  <input
                                      type="radio"
                                      name="images[]"
                                      id="image_{{ $image->id }}"
                                      value="{{ $image->id }}"
                                      class="d-none"
                                      {{ $post->images()->get()->contains($image->id) ? "checked" : "" }}
                                  >
                                  <label for="image_{{ $image->id }}"><img src="/storage/{{ $image->filepath }}" alt="{{ $image->alt }}" image_id="{{ $image->id }}"></label>
                                  @empty
                                  <h4>Изображения не найдены</h4>
                                  @endforelse
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="is_published" {{ $post->is_published ? "checked" : "" }}>
                            <span class="form-check-sign">
                              <span class="check"></span>
                            </span>
                            <p>Опубликовано</p>
                          </label>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Дата публикации</label>
                        <input type="date" class="form-control" name="publish_date" value="{{ explode(" ", $post->publish_time)[0] }}">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Время публикации</label>
                        <input type="time" class="form-control" name="publish_time" value="{{ @explode(" ", $post->publish_time)[1] }}">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Дата снятия с публикации</label>
                        <input type="date" class="form-control" name="unpublish_date" value="{{ explode(" ", $post->unpublish_time)[0] }}">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Время снятия с публикации</label>
                        <input type="time" class="form-control" name="unpublish_time" value="{{ @explode(" ", $post->unpublish_time)[1] }}">
                      </div>
                    </div>
                  </div>
              </div>
            </div>
              <div class="collapse show" id="collapseExample">
                  <div class="card card-body">
                      <div class="form-group">
                          <label class="bmd-label-floating">Количество недавно добавленных постов</label>
                          <input
                              type="number"
                              class="form-control"
                              name="recent_posts_count"
                              value="{{ isset($post->settings->recent_posts_count) ? $post->settings->recent_posts_count : RECENT_POSTS_COUNT }}">
                      </div>
                      <div class="form-group">
                          <label class="bmd-label-floating">Количество избранных постов</label>
                          <input
                              type="number"
                              class="form-control"
                              name="featured_posts_count"
                              value="{{ isset($post->settings->featured_posts_count) ? $post->settings->featured_posts_count : FEATURED_POSTS_COUNT }}">
                      </div>
                      <div class="form-group">
                          <label class="bmd-label-floating">Количество тегов</label>
                          <input
                              type="number"
                              class="form-control"
                              name="tags_count"
                              value="{{ isset($post->settings->tags_count) ? $post->settings->tags_count : TAGS_COUNT }}">
                      </div>
                      <div class="form-group">
                          <label class="bmd-label-floating">Количество категорий</label>
                          <input
                              type="number"
                              class="form-control"
                              name="categories_count"
                              value="{{ isset($post->settings->categories_count) ? $post->settings->categories_count : CATEGORIES_COUNT }}">
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
  </form>
</div>
@endsection
