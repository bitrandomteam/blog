<div class="col-md-3 p-0">
    <div class="category" style="background-image: url({{ ($category->images()->first()) ? asset("/storage/" . $category->images()->first()->filepath) : "/storage/images/no-photo.png" }})">
        <a href="{{ route("categories.show", $category->slug) }}">
            <h2 class="category__header">{{ $category->header }}</h2>
        </a>
    </div>
</div>
