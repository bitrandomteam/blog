@push('styles')
    <link rel="stylesheet" href="{{ asset('/css/public/newsletter.css') }}">
@endpush

<div class="newsletter" id="newsletter">
<h4><span>&nbsp;</span> Подписаться</h4>
    @if ($errors)
        @foreach($errors->all() as $error)
            <div class="error">
                {{ $error }}
            </div>
        @endforeach
    @endif
    <form id="form-wysija-2" method="post" action="{{ route('subscribe.new') }}" class="widget_wysija">
        @csrf
        <p class="wysija-paragraph">
            <input type="email" name="email" class="wysija-input" title="Email" placeholder="Электронная  почта..." value="">
        </p>
        <input class="d-none" type="submit" value="Subscribe!">
        <p>Подпишись на еженедельные посты и не пропусти ничего интересного.</p>
    </form>
</div>
