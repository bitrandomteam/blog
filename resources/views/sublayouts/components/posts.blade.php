@push('styles')
    <link rel="stylesheet" href="{{ asset('/css/public/posts.css') }}">
@endpush

<div class="posts mb-5">
@forelse($posts as $post)
    <div class="post">
        <div class="post__image">
            <a href="{{ route("posts.show", $post->slug) }}">
                <img src="{{ asset('/storage/' . $post->getImage()->filepath) }}" alt="{{ $post->getImage()->alt }}">
            </a>
        </div>
        <div class="post__header justify-content-center">
            <h2><a href="{{ route("posts.show", $post->slug) }}">{{ $post->header }}</a></h2>
        </div>
        <div class="post__info d-flex justify-content-center">
            <div class="post__info post__info_category">
                {{ $post->category()->first()->header }}
            </div>
            <div class="post__info post__info_date">
                {{ ($post->publish_date !== null) ? \Carbon\Carbon::parse($post->publish_date)->format("d M, Y") : \Carbon\Carbon::parse($post->created_at)->format("d M, Y") }}
            </div>
        </div>
        <div class="post__description">
            {!! $post->description !!}
        </div>
        <div class="post__continue-reading">
            <a href="{{ route("posts.show", $post->slug) }}">Читать полностью</a>
        </div>
    </div>
@empty
    <p>Публикации не найдены...</p>
@endforelse
</div>
