@push('styles')
    <link rel="stylesheet" href="{{ asset('/css/public/breadcrumbs.css') }}">
@endpush

<ol class="breadcrumbs d-flex justify-content-center mr-5">
    <li><a href="/">Главная</a></li>
    <li class="active">{{ $page->header }}</li>
</ol>
