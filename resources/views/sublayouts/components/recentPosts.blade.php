@push('styles')
    <link rel="stylesheet" href="{{ asset('/css/public/recent_posts.css') }}">
@endpush

<div class="recent__posts">
    <h4><span>&nbsp;</span> Новые публикации</h4>
@forelse ($recentPosts as $post)
    <div class="recent__post">
        <div class="recent__post__image">
            <a href="{{ route("posts.show", $post->slug) }}">
                <img src="{{ asset('/storage/' . $post->images()->first()->filepath) }}" alt="{{ $post->images()->first()->alt }}">
            </a>
        </div>
        <div class="recent__post__info">
            <div class="recent__post__info recent__post__info_header">
                <h5><a href="{{ route("posts.show", $post->slug) }}">{{ $post->header }}</a></h5>
            </div>
            <div class="recent__post__info recent__post__info_description">
                {!! $post->description !!}
            </div>
        </div>
    </div>
@empty
@endforelse
</div>
