@push('styles')
    <link rel="stylesheet" href="{{ asset('/css/public/subcategories.css') }}">
@endpush

@if ($subcategories->count())
    @if (isset($text_categories))
        <h4><span>&nbsp;</span> {{ $text_categories }}</h4>
    @else
        <h4><span>&nbsp;</span> Подкатегории</h4>
    @endif

<div class="subcategories">
    @forelse($subcategories as $subcategory)
        <div class="subcategory">
            <a class="d-flex justify-content-between" href="{{ route("categories.show", $subcategory->slug) }}">{{ $subcategory->header }} <span>{{ $subcategory->posts()->count() }}</span></a>
        </div>
    @empty

    @endforelse
</div>
@endif
