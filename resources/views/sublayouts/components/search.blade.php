<div class="search">
    <input id="search__checkbox" type="checkbox" class="d-none">
    <label for="search__checkbox" class="search__icon">
        <img src="{{ asset('/images/public/icons/search.png') }}" alt="Search">
    </label>
    <div class="search__input">
        <input type="text" class="form-control" placeholder="Поиск...">
    </div>
</div>
