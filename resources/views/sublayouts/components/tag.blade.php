<div class="col-md-3 p-0">
    <div class="tag">
        <a href="{{ route("tags.show", $tag->slug) }}">
            <h2 class="tag__header">{{ $tag->header }}</h2>
        </a>
    </div>
</div>
