@push('styles')
    <link rel="stylesheet" href="{{ asset('/css/public/tags.css') }}">
@endpush

<h4><span>&nbsp;</span> Теги</h4>
<div class="tags">
    @forelse($tags as $tag)
        <div class="tag">
            <a href="{{ route("tags.show", $tag->slug) }}">{{ $tag->header }}</a>
        </div>
    @empty

    @endforelse
</div>
