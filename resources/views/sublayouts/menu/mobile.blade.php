<div class="menu__mobile d-block d-md-none">
    <input id="menu__mobile__checkbox" type="checkbox" class="d-none">
    <label for="menu__mobile__checkbox">
        <img src="{{ asset("/images/public/icons/menu.png") }}" alt="Menu">
    </label>
    <div class="menu">
        <div class="row">
            <div class="col-12 d-flex justify-content-end">
                <label for="menu__mobile__checkbox">
                    <span class="close">
                        <span class="first-line"></span>
                        <span class="second-line"></span>
                    </span>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-12 justify-content-center">
                <ul class="p-0">
                    @foreach ($pages as $page)
                        @if ($page->show_in_menu)
                            <li class="menu__top__item">
                                <a href="{{ route("pages.show", $page->slug) }}">{{ $page->header }}</a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>

        </div>


    </div>
</div>
