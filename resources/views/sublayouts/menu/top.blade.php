<ul class="menu__top d-md-flex d-none">
    @foreach ($pages as $page)
        @if ($page->show_in_menu)
            <li class="menu__top__item">
                <a href="{{ route("pages.show", $page->slug) }}">{{ $page->header }}</a>
            </li>
        @endif
    @endforeach
</ul>
