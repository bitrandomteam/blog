-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 04 2019 г., 01:07
-- Версия сервера: 5.6.37
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `blog-ks`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `header` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish_time` timestamp NULL DEFAULT NULL,
  `unpublish_time` timestamp NULL DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `settings` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `template_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `header`, `slug`, `seo_title`, `seo_keywords`, `seo_description`, `description`, `publish_time`, `unpublish_time`, `is_published`, `parent_id`, `created_at`, `updated_at`, `settings`, `type`, `template_id`) VALUES
(1, 'Lifestyle', 'ls', NULL, NULL, NULL, '<p>new description</p>', '2019-09-02 21:00:00', '2019-09-04 05:55:00', 1, 2, '2019-09-01 10:56:40', '2019-09-03 16:26:10', '{\"posts_count_on_page\":10,\"recent_posts_count\":4,\"featured_posts_count\":3,\"tags_count\":8,\"subcategories_count\":5}', 'user', 3),
(2, 'Psycho', 'psycho', NULL, NULL, NULL, '<p>dd</p>', NULL, NULL, 1, 4, '2019-09-02 07:26:20', '2019-09-03 13:26:16', '{\"scalar\":\"\",\"posts_count_on_page\":10,\"featured_posts_count\":3,\"tags_count\":8,\"subcategories_count\":5}', 'user', NULL),
(4, 'Root', 'category', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-09-03 13:01:43', '2019-09-03 13:01:43', '{\"posts_count_on_page\":10,\"featured_posts_count\":3,\"tags_count\":8,\"subcategories_count\":5}', 'system', NULL),
(6, 'Test', 'test2', NULL, NULL, NULL, '<p>tesadfwadawdawd</p>', NULL, NULL, 1, 1, '2019-09-03 13:47:14', '2019-09-03 14:06:41', '{\"posts_count_on_page\":10,\"recent_posts_count\":20,\"featured_posts_count\":3,\"tags_count\":8,\"subcategories_count\":5}', 'user', NULL),
(7, 'wdawd', 'wadawd', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-09-03 16:26:38', '2019-09-03 16:26:38', '{\"posts_count_on_page\":10,\"recent_posts_count\":4,\"featured_posts_count\":3,\"tags_count\":8,\"subcategories_count\":5}', 'user', 6);

-- --------------------------------------------------------

--
-- Структура таблицы `category_image`
--

CREATE TABLE `category_image` (
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `header` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filepath` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `images`
--

INSERT INTO `images` (`id`, `header`, `alt`, `filepath`, `created_at`, `updated_at`) VALUES
(1, 'City', 'night city', 'images/c4joCMAoq2fWC0g91WQJc1PDWLzXoZ6h6qQezhLg.jpeg', '2019-09-01 10:52:03', '2019-09-03 09:36:32'),
(2, 'photo-1541233349642-6e425fe6190e.jpg', 'photo-1541233349642-6e425fe6190e.jpg', 'images/qvhg1DL0GluYb2LVIwf8wQ7lPsV92haiXq8gL0oG.jpeg', '2019-09-01 10:52:03', '2019-09-01 10:52:03'),
(3, 'Sample-jpg-image-500kb.jpg', 'Sample-jpg-image-500kb.jpg', 'images/jvDLS0HmbpZZi3Q8Pgn60MiRZWf92yP1IHpkxv75.jpeg', '2019-09-01 10:52:03', '2019-09-01 10:52:03');

-- --------------------------------------------------------

--
-- Структура таблицы `image_page`
--

CREATE TABLE `image_page` (
  `page_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `image_post`
--

CREATE TABLE `image_post` (
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ip_addresses`
--

CREATE TABLE `ip_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `ip_addresses`
--

INSERT INTO `ip_addresses` (`id`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', '2019-09-01 06:04:23', '2019-09-01 06:04:23');

-- --------------------------------------------------------

--
-- Структура таблицы `ip_address_page`
--

CREATE TABLE `ip_address_page` (
  `page_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `ip_address_page`
--

INSERT INTO `ip_address_page` (`page_id`, `ip_address_id`, `created_at`, `updated_at`) VALUES
(4, 1, '2019-09-01 06:04:23', '2019-09-01 06:04:23'),
(5, 1, '2019-09-02 14:19:36', '2019-09-02 14:19:36');

-- --------------------------------------------------------

--
-- Структура таблицы `ip_address_post`
--

CREATE TABLE `ip_address_post` (
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_24_133619_create_tags_table', 1),
(4, '2019_08_24_153904_create_categories_table', 1),
(5, '2019_08_24_160617_update_categories_table', 1),
(6, '2019_08_24_161619_create_posts_table', 1),
(7, '2019_08_24_161935_posts_table_update', 1),
(8, '2019_08_24_184728_create_images_table', 1),
(9, '2019_08_25_172158_create_category_image_table', 1),
(10, '2019_08_25_195042_add_foreign_into_posts', 1),
(11, '2019_08_25_195313_add_tag_relation_to_post_table', 1),
(12, '2019_08_25_195527_add_image_relation_to_post_table', 1),
(13, '2019_08_27_184532_create_pages_table', 1),
(14, '2019_08_27_185101_create_pages_images_table', 1),
(15, '2019_08_27_193658_update_page_table', 1),
(16, '2019_08_27_195947_create_page_ip_addresses_table', 1),
(17, '2019_08_27_200244_page_ip_addresses', 1),
(18, '2019_09_01_080825_create_templates_table', 1),
(19, '2019_09_01_083457_add_template_foreign_into_pages_table', 1),
(21, '2019_09_01_084625_update_templates_table', 2),
(22, '2019_09_02_153655_pages_table_update', 3),
(23, '2019_09_02_165859_add_views_to_posts', 4),
(26, '2019_09_02_170055_posts_ip_addresses', 5),
(27, '2019_09_02_170748_add_parameter_show_in_menu_to_pages', 5),
(28, '2019_09_02_172038_add_menu_ordering_to_pages', 6),
(29, '2019_09_03_153329_add_settings_to_categories', 7),
(30, '2019_09_03_155124_add_type_of_category', 8),
(31, '2019_09_03_163953_delete_publish_field_from_tags', 9),
(32, '2019_09_03_164359_add_settings_to_posts', 10),
(33, '2019_09_03_191247_add_template_foreign_to_category', 11),
(34, '2019_09_03_191342_add_template_foreign_to_posts', 12),
(35, '2019_09_03_191408_add_template_foreign_to_tags', 13);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `header` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_time` timestamp NULL DEFAULT NULL,
  `unpublish_time` timestamp NULL DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `views` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `template_id` bigint(20) UNSIGNED DEFAULT NULL,
  `settings` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_in_menu` tinyint(1) NOT NULL DEFAULT '0',
  `menu_ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `header`, `slug`, `seo_title`, `seo_keywords`, `seo_description`, `description`, `text`, `publish_time`, `unpublish_time`, `is_published`, `created_at`, `updated_at`, `views`, `template_id`, `settings`, `show_in_menu`, `menu_ordering`) VALUES
(4, 'Главная', 'main', 'Главная', 'ключи', 'мета описание', 'описание', 'текст', NULL, NULL, 1, '2019-09-01 06:04:03', '2019-09-03 16:36:09', 1, 4, '{\"posts_count_on_page\":10,\"recent_posts_count\":4,\"featured_posts_count\":3,\"tags_count\":8,\"categories_count\":5}', 1, 0),
(5, 'Категория', 'categories', NULL, NULL, NULL, NULL, 'Categories', NULL, NULL, 1, '2019-09-02 14:18:50', '2019-09-02 14:19:36', 1, 3, NULL, 1, 1),
(6, 'Test', 'test23', NULL, NULL, NULL, NULL, 'tesss', '2019-09-03 21:00:00', '2019-09-08 09:23:00', 1, '2019-09-03 15:58:40', '2019-09-03 16:08:48', 0, 3, '{\"recent_posts_count\":4215,\"featured_posts_count\":3152,\"tags_count\":8125,\"categories_count\":5125}', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `header` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_time` timestamp NULL DEFAULT NULL,
  `unpublish_time` timestamp NULL DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `views` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `settings` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `template_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `created_at`, `updated_at`, `header`, `slug`, `seo_title`, `seo_keywords`, `seo_description`, `description`, `text`, `publish_time`, `unpublish_time`, `is_published`, `category_id`, `views`, `settings`, `template_id`) VALUES
(1, '2019-09-01 10:54:24', '2019-09-03 16:32:05', 'First post', 'post1', NULL, NULL, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aut est eveniet quaerat quos, soluta vel veritatis. Amet harum hic labore perferendis, placeat quas quo veniam. Adipisci eos et fugit ipsum natus obcaecati similique sit voluptate! Aliquam animi assumenda culpa, cum dolorem doloremque doloribus eligendi enim eum expedita facilis id illo in</p>', '<p>Test</p>', NULL, NULL, 1, 1, 0, '{\"recent_posts_count\":4,\"featured_posts_count\":3,\"tags_count\":8,\"categories_count\":5}', 5),
(2, '2019-09-02 07:32:46', '2019-09-02 14:58:08', 'Second post', 'sec-post', NULL, NULL, NULL, NULL, 'sec post', NULL, NULL, 1, 1, 0, '', NULL),
(3, '2019-09-03 14:04:30', '2019-09-03 16:09:55', 'My firsrt post', 'post12', 'Seo title', 'Seo keywords', 'Seo desc', '<p>First post desc</p>', '<p>My first post text</p>', '2019-09-03 20:23:00', '2019-09-12 09:23:00', 1, 2, 0, '{\"recent_posts_count\":4000,\"featured_posts_count\":32,\"tags_count\":82,\"categories_count\":5}', NULL),
(4, '2019-09-03 16:32:26', '2019-09-03 16:32:26', 'awdawdawdawd', 'adwdawdaw123', NULL, NULL, NULL, '<p>awdawdawd</p>', '<p>awdawdawd</p>', NULL, NULL, 1, 1, 0, '{\"recent_posts_count\":4,\"featured_posts_count\":3,\"tags_count\":8,\"categories_count\":5}', 6);

-- --------------------------------------------------------

--
-- Структура таблицы `post_tag`
--

CREATE TABLE `post_tag` (
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `tag_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `header` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `template_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tags`
--

INSERT INTO `tags` (`id`, `header`, `slug`, `seo_title`, `seo_keywords`, `seo_description`, `description`, `created_at`, `updated_at`, `template_id`) VALUES
(1, 'My first tag', 'tag', NULL, NULL, NULL, NULL, '2019-09-02 15:06:38', '2019-09-03 13:38:47', NULL),
(2, 'Second tag', 'sec-tag', 'seo title', 'seo keywords', 'seo desc', '<p>teg desc</p>', '2019-09-03 13:39:22', '2019-09-03 16:29:23', 4),
(3, 'awdawd', 'wadawdaw', NULL, NULL, NULL, NULL, '2019-09-03 16:29:46', '2019-09-03 16:29:54', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `templates`
--

CREATE TABLE `templates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `templates`
--

INSERT INTO `templates` (`id`, `name`, `filename`, `created_at`, `updated_at`) VALUES
(3, 'Главная (вариант 1)', 'home', '2019-08-31 21:00:00', '2019-08-31 21:00:00'),
(4, 'Главная (вариант 2)', 'home2', '2019-08-31 21:00:00', '2019-08-31 21:00:00'),
(5, 'Главная (вариант 3)', 'home3', '2019-08-31 21:00:00', '2019-08-31 21:00:00'),
(6, 'Главная (вариант 4)', 'home4', '2019-08-31 21:00:00', '2019-08-31 21:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Vladislav', 'random.person.1907@gmail.loc', NULL, '$2y$10$s3KRAwTek.0oHAEnZkw5mOFI/UMBr/I3Qr1CLwll7UnEgI1tYM.K.', NULL, '2019-09-01 10:56:19', '2019-09-01 10:56:19');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`),
  ADD KEY `categories_template_id_foreign` (`template_id`);

--
-- Индексы таблицы `category_image`
--
ALTER TABLE `category_image`
  ADD KEY `category_image_category_id_foreign` (`category_id`),
  ADD KEY `category_image_image_id_foreign` (`image_id`);

--
-- Индексы таблицы `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `image_page`
--
ALTER TABLE `image_page`
  ADD KEY `image_page_page_id_foreign` (`page_id`),
  ADD KEY `image_page_image_id_foreign` (`image_id`);

--
-- Индексы таблицы `image_post`
--
ALTER TABLE `image_post`
  ADD KEY `image_post_post_id_foreign` (`post_id`),
  ADD KEY `image_post_image_id_foreign` (`image_id`);

--
-- Индексы таблицы `ip_addresses`
--
ALTER TABLE `ip_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ip_address_page`
--
ALTER TABLE `ip_address_page`
  ADD KEY `ip_address_page_page_id_foreign` (`page_id`),
  ADD KEY `ip_address_page_ip_address_id_foreign` (`ip_address_id`);

--
-- Индексы таблицы `ip_address_post`
--
ALTER TABLE `ip_address_post`
  ADD KEY `ip_address_post_post_id_foreign` (`post_id`),
  ADD KEY `ip_address_post_ip_address_id_foreign` (`ip_address_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pages_template_id_foreign` (`template_id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_category_id_foreign` (`category_id`),
  ADD KEY `posts_template_id_foreign` (`template_id`);

--
-- Индексы таблицы `post_tag`
--
ALTER TABLE `post_tag`
  ADD KEY `post_tag_post_id_foreign` (`post_id`),
  ADD KEY `post_tag_tag_id_foreign` (`tag_id`);

--
-- Индексы таблицы `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tags_template_id_foreign` (`template_id`);

--
-- Индексы таблицы `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `ip_addresses`
--
ALTER TABLE `ip_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `templates`
--
ALTER TABLE `templates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `categories_template_id_foreign` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id`);

--
-- Ограничения внешнего ключа таблицы `category_image`
--
ALTER TABLE `category_image`
  ADD CONSTRAINT `category_image_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_image_image_id_foreign` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `image_page`
--
ALTER TABLE `image_page`
  ADD CONSTRAINT `image_page_image_id_foreign` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `image_page_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `image_post`
--
ALTER TABLE `image_post`
  ADD CONSTRAINT `image_post_image_id_foreign` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `image_post_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ip_address_page`
--
ALTER TABLE `ip_address_page`
  ADD CONSTRAINT `ip_address_page_ip_address_id_foreign` FOREIGN KEY (`ip_address_id`) REFERENCES `ip_addresses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ip_address_page_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ip_address_post`
--
ALTER TABLE `ip_address_post`
  ADD CONSTRAINT `ip_address_post_ip_address_id_foreign` FOREIGN KEY (`ip_address_id`) REFERENCES `ip_addresses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ip_address_post_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_template_id_foreign` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id`);

--
-- Ограничения внешнего ключа таблицы `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_template_id_foreign` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id`);

--
-- Ограничения внешнего ключа таблицы `post_tag`
--
ALTER TABLE `post_tag`
  ADD CONSTRAINT `post_tag_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_template_id_foreign` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
