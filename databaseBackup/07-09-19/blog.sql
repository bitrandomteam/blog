-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 07 2019 г., 09:12
-- Версия сервера: 5.6.37
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `blog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_categories`
--

CREATE TABLE `sefi95_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `header` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish_time` timestamp NULL DEFAULT NULL,
  `unpublish_time` timestamp NULL DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `settings` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `template_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_category_image`
--

CREATE TABLE `sefi95_category_image` (
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_images`
--

CREATE TABLE `sefi95_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `header` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filepath` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_image_page`
--

CREATE TABLE `sefi95_image_page` (
  `page_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_image_post`
--

CREATE TABLE `sefi95_image_post` (
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_ip_addresses`
--

CREATE TABLE `sefi95_ip_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_ip_address_page`
--

CREATE TABLE `sefi95_ip_address_page` (
  `page_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_ip_address_post`
--

CREATE TABLE `sefi95_ip_address_post` (
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_migrations`
--

CREATE TABLE `sefi95_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `sefi95_migrations`
--

INSERT INTO `sefi95_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_24_133619_create_tags_table', 1),
(4, '2019_08_24_153904_create_categories_table', 1),
(5, '2019_08_24_160617_update_categories_table', 1),
(6, '2019_08_24_161619_create_posts_table', 1),
(7, '2019_08_24_161935_posts_table_update', 1),
(8, '2019_08_24_184728_create_images_table', 1),
(9, '2019_08_25_172158_create_category_image_table', 1),
(10, '2019_08_25_195042_add_foreign_into_posts', 1),
(11, '2019_08_25_195313_add_tag_relation_to_post_table', 1),
(12, '2019_08_25_195527_add_image_relation_to_post_table', 1),
(13, '2019_08_27_184532_create_pages_table', 1),
(14, '2019_08_27_185101_create_pages_images_table', 1),
(15, '2019_08_27_193658_update_page_table', 1),
(16, '2019_08_27_195947_create_page_ip_addresses_table', 1),
(17, '2019_08_27_200244_page_ip_addresses', 1),
(18, '2019_09_01_080825_create_templates_table', 1),
(19, '2019_09_01_083457_add_template_foreign_into_pages_table', 1),
(20, '2019_09_01_084625_update_templates_table', 1),
(21, '2019_09_02_153655_pages_table_update', 1),
(22, '2019_09_02_165859_add_views_to_posts', 1),
(23, '2019_09_02_170055_posts_ip_addresses', 1),
(24, '2019_09_02_170748_add_parameter_show_in_menu_to_pages', 1),
(25, '2019_09_02_172038_add_menu_ordering_to_pages', 1),
(26, '2019_09_03_153329_add_settings_to_categories', 1),
(27, '2019_09_03_155124_add_type_of_category', 1),
(28, '2019_09_03_163953_delete_publish_field_from_tags', 1),
(29, '2019_09_03_164359_add_settings_to_posts', 1),
(30, '2019_09_03_191247_add_template_foreign_to_category', 1),
(31, '2019_09_03_191342_add_template_foreign_to_posts', 1),
(32, '2019_09_03_191408_add_template_foreign_to_tags', 1),
(33, '2019_09_05_105449_add_settings_to_tags', 1),
(34, '2019_09_05_203913_delete_description_from_pages', 1),
(35, '2019_09_07_042503_create_subscribers_table', 1),
(36, '2019_09_07_050731_add_code_field_to_subscribers', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_pages`
--

CREATE TABLE `sefi95_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `header` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_time` timestamp NULL DEFAULT NULL,
  `unpublish_time` timestamp NULL DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `views` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `template_id` bigint(20) UNSIGNED DEFAULT NULL,
  `settings` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_in_menu` tinyint(1) NOT NULL DEFAULT '0',
  `menu_ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_password_resets`
--

CREATE TABLE `sefi95_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_posts`
--

CREATE TABLE `sefi95_posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `header` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_time` timestamp NULL DEFAULT NULL,
  `unpublish_time` timestamp NULL DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `views` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `settings` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `template_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_post_tag`
--

CREATE TABLE `sefi95_post_tag` (
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `tag_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_subscribers`
--

CREATE TABLE `sefi95_subscribers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_tags`
--

CREATE TABLE `sefi95_tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `header` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `template_id` bigint(20) UNSIGNED DEFAULT NULL,
  `settings` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_templates`
--

CREATE TABLE `sefi95_templates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sefi95_users`
--

CREATE TABLE `sefi95_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `sefi95_categories`
--
ALTER TABLE `sefi95_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sefi95_categories_parent_id_foreign` (`parent_id`),
  ADD KEY `sefi95_categories_template_id_foreign` (`template_id`);

--
-- Индексы таблицы `sefi95_category_image`
--
ALTER TABLE `sefi95_category_image`
  ADD KEY `sefi95_category_image_category_id_foreign` (`category_id`),
  ADD KEY `sefi95_category_image_image_id_foreign` (`image_id`);

--
-- Индексы таблицы `sefi95_images`
--
ALTER TABLE `sefi95_images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sefi95_image_page`
--
ALTER TABLE `sefi95_image_page`
  ADD KEY `sefi95_image_page_page_id_foreign` (`page_id`),
  ADD KEY `sefi95_image_page_image_id_foreign` (`image_id`);

--
-- Индексы таблицы `sefi95_image_post`
--
ALTER TABLE `sefi95_image_post`
  ADD KEY `sefi95_image_post_post_id_foreign` (`post_id`),
  ADD KEY `sefi95_image_post_image_id_foreign` (`image_id`);

--
-- Индексы таблицы `sefi95_ip_addresses`
--
ALTER TABLE `sefi95_ip_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sefi95_ip_address_page`
--
ALTER TABLE `sefi95_ip_address_page`
  ADD KEY `sefi95_ip_address_page_page_id_foreign` (`page_id`),
  ADD KEY `sefi95_ip_address_page_ip_address_id_foreign` (`ip_address_id`);

--
-- Индексы таблицы `sefi95_ip_address_post`
--
ALTER TABLE `sefi95_ip_address_post`
  ADD KEY `sefi95_ip_address_post_post_id_foreign` (`post_id`),
  ADD KEY `sefi95_ip_address_post_ip_address_id_foreign` (`ip_address_id`);

--
-- Индексы таблицы `sefi95_migrations`
--
ALTER TABLE `sefi95_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sefi95_pages`
--
ALTER TABLE `sefi95_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sefi95_pages_template_id_foreign` (`template_id`);

--
-- Индексы таблицы `sefi95_password_resets`
--
ALTER TABLE `sefi95_password_resets`
  ADD KEY `sefi95_password_resets_email_index` (`email`);

--
-- Индексы таблицы `sefi95_posts`
--
ALTER TABLE `sefi95_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sefi95_posts_category_id_foreign` (`category_id`),
  ADD KEY `sefi95_posts_template_id_foreign` (`template_id`);

--
-- Индексы таблицы `sefi95_post_tag`
--
ALTER TABLE `sefi95_post_tag`
  ADD KEY `sefi95_post_tag_post_id_foreign` (`post_id`),
  ADD KEY `sefi95_post_tag_tag_id_foreign` (`tag_id`);

--
-- Индексы таблицы `sefi95_subscribers`
--
ALTER TABLE `sefi95_subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sefi95_tags`
--
ALTER TABLE `sefi95_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sefi95_tags_template_id_foreign` (`template_id`);

--
-- Индексы таблицы `sefi95_templates`
--
ALTER TABLE `sefi95_templates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sefi95_users`
--
ALTER TABLE `sefi95_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sefi95_users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `sefi95_categories`
--
ALTER TABLE `sefi95_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `sefi95_images`
--
ALTER TABLE `sefi95_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `sefi95_ip_addresses`
--
ALTER TABLE `sefi95_ip_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `sefi95_migrations`
--
ALTER TABLE `sefi95_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT для таблицы `sefi95_pages`
--
ALTER TABLE `sefi95_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `sefi95_posts`
--
ALTER TABLE `sefi95_posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `sefi95_subscribers`
--
ALTER TABLE `sefi95_subscribers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `sefi95_tags`
--
ALTER TABLE `sefi95_tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `sefi95_templates`
--
ALTER TABLE `sefi95_templates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `sefi95_users`
--
ALTER TABLE `sefi95_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `sefi95_categories`
--
ALTER TABLE `sefi95_categories`
  ADD CONSTRAINT `sefi95_categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `sefi95_categories` (`id`),
  ADD CONSTRAINT `sefi95_categories_template_id_foreign` FOREIGN KEY (`template_id`) REFERENCES `sefi95_templates` (`id`);

--
-- Ограничения внешнего ключа таблицы `sefi95_category_image`
--
ALTER TABLE `sefi95_category_image`
  ADD CONSTRAINT `sefi95_category_image_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `sefi95_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sefi95_category_image_image_id_foreign` FOREIGN KEY (`image_id`) REFERENCES `sefi95_images` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sefi95_image_page`
--
ALTER TABLE `sefi95_image_page`
  ADD CONSTRAINT `sefi95_image_page_image_id_foreign` FOREIGN KEY (`image_id`) REFERENCES `sefi95_images` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sefi95_image_page_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `sefi95_pages` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sefi95_image_post`
--
ALTER TABLE `sefi95_image_post`
  ADD CONSTRAINT `sefi95_image_post_image_id_foreign` FOREIGN KEY (`image_id`) REFERENCES `sefi95_images` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sefi95_image_post_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `sefi95_posts` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sefi95_ip_address_page`
--
ALTER TABLE `sefi95_ip_address_page`
  ADD CONSTRAINT `sefi95_ip_address_page_ip_address_id_foreign` FOREIGN KEY (`ip_address_id`) REFERENCES `sefi95_ip_addresses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sefi95_ip_address_page_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `sefi95_pages` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sefi95_ip_address_post`
--
ALTER TABLE `sefi95_ip_address_post`
  ADD CONSTRAINT `sefi95_ip_address_post_ip_address_id_foreign` FOREIGN KEY (`ip_address_id`) REFERENCES `sefi95_ip_addresses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sefi95_ip_address_post_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `sefi95_posts` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sefi95_pages`
--
ALTER TABLE `sefi95_pages`
  ADD CONSTRAINT `sefi95_pages_template_id_foreign` FOREIGN KEY (`template_id`) REFERENCES `sefi95_templates` (`id`);

--
-- Ограничения внешнего ключа таблицы `sefi95_posts`
--
ALTER TABLE `sefi95_posts`
  ADD CONSTRAINT `sefi95_posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `sefi95_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sefi95_posts_template_id_foreign` FOREIGN KEY (`template_id`) REFERENCES `sefi95_templates` (`id`);

--
-- Ограничения внешнего ключа таблицы `sefi95_post_tag`
--
ALTER TABLE `sefi95_post_tag`
  ADD CONSTRAINT `sefi95_post_tag_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `sefi95_posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sefi95_post_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `sefi95_tags` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sefi95_tags`
--
ALTER TABLE `sefi95_tags`
  ADD CONSTRAINT `sefi95_tags_template_id_foreign` FOREIGN KEY (`template_id`) REFERENCES `sefi95_templates` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
