const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/main.scss', 'public/css/public')
    .sass('resources/sass/menu.scss', 'public/css/public')
    .sass('resources/sass/search.scss', 'public/css/public')
    .sass('resources/sass/footer.scss', 'public/css/public')
    .sass('resources/sass/categories.scss', 'public/css/public')
    .sass('resources/sass/posts.scss', 'public/css/public')
    .sass('resources/sass/recent_posts.scss', 'public/css/public')
    .sass('resources/sass/tags.scss', 'public/css/public')
    .sass('resources/sass/newsletter.scss', 'public/css/public')
    .sass('resources/sass/subcategories.scss', 'public/css/public')
    .sass('resources/sass/home.scss', 'public/css/public')
    .sass('resources/sass/page.scss', 'public/css/public')
    .sass('resources/sass/breadcrumbs.scss', 'public/css/public');

var LiveReloadPlugin = require('webpack-livereload-plugin');

mix.webpackConfig({
    plugins: [
        new LiveReloadPlugin()
    ]
});
