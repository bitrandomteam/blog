<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use App\Model;
use App\Post;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    $paragraphsCount = rand(5, 30);

    $text = '';
    for ($i = 0; $i < $paragraphsCount; $i++) {
        $text .= "<p>" . $faker->paragraph(rand(3, 10)) . "</p>";
    }

    return [
        'header' => $faker->unique()->sentence(rand(3,7)),
        'slug' => $faker->unique()->slug(),
        'seo_title' => $faker->unique()->sentence(rand(3,7)),
        'seo_keywords' => $faker->unique()->words(rand(4,12), true),
        'seo_description' => $faker->unique()->text(255),
        'description' => "<p>" . $faker->paragraphs(2, true) . "</p>",
        'text' => $text,
        'publish_time' => null,
        'unpublish_time' => null,
        'is_published' => 1,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
        'template_id' => 1,
        'settings' => "{\"posts_count_on_page\":10,\"recent_posts_count\":4,\"featured_posts_count\":3,\"tags_count\":8,\"subcategories_count\":5,\"categories_count\":5}",
        'category_id' => function() {
            return Category::inRandomOrder()->first()->id;
        }
    ];
});
