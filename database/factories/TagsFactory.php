<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Tag;
use App\Template;
use Faker\Generator as Faker;

$factory->define(Tag::class, function (Faker $faker) {
    return [
        "header" => $faker->unique()->word(),
        "slug" => $faker->unique()->slug(),
        "seo_title" => $faker->sentence(rand(3, 6)),
        "seo_keywords" => $faker->words(rand(5,8), true),
        "seo_description" => $faker->text(180),
        "description" => $faker->paragraphs(3, true),
        "settings" => "{\"posts_count_on_page\":10,\"recent_posts_count\":4,\"featured_posts_count\":3,\"tags_count\":8,\"subcategories_count\":5,\"categories_count\":5}",
        "template_id" => function() {
            return Template::find(2);
        }
    ];
});
