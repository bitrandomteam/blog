<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Template;
use Faker\Generator as Faker;

$factory->define(Template::class, function (Faker $faker) {
    return [
        [
            'name' => 'Публикация',
            'filename' => 'post'
        ],
        [
            'name' => 'Страница одного тега',
            'filename' => 'tag'
        ],
        [
            'name' => 'Страница одной категории',
            'filename' => 'category'
        ],
        [
            'name' => 'Статичная страница',
            'filename' => 'page'
        ],
        [
            'name' => 'Список категорий',
            'filename' => 'categories'
        ],
        [
            'name' => 'Список тегов',
            'filename' => 'tags'
        ],
        [
            'name' => 'Главная',
            'filename' => 'home'
        ]
    ];
});
