<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostsTableUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('header', 255);
            $table->string('slug', 255);
            $table->string('seo_title', 255)->nullable();
            $table->string('seo_keywords', 500)->nullable();
            $table->string('seo_description', 500)->nullable();
            $table->string('description', 10000)->nullable();
            $table->text('text');
            $table->timestamp('publish_time')->nullable();
            $table->timestamp('unpublish_time')->nullable();
            $table->boolean('is_published')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
