<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTemplateForeignIntoPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages', function(Blueprint $table) {
           $table->bigInteger("template_id")->unsigned()->nullable();
        });

        Schema::table('pages', function(Blueprint $table) {
            $table->foreign('template_id')->references('id')->on('templates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('pages', 'template_id')) {
            Schema::table('pages', function (Blueprint $table) {
                $table->dropForeign('template_id');
                $table->dropColumn('template_id');
            });
        }
    }
}
