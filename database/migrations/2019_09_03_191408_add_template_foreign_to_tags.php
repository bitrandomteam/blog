<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTemplateForeignToTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tags', function(Blueprint $table) {
            $table->bigInteger("template_id")->unsigned()->nullable();
        });

        Schema::table('tags', function(Blueprint $table) {
            $table->foreign('template_id')->references('id')->on('templates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('tags', 'template_id')) {
            Schema::table('tags', function (Blueprint $table) {
                $table->dropForeign('template_id');
                $table->dropColumn('template_id');
            });
        }
    }
}
