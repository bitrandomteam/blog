<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTemplateForeignToCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function(Blueprint $table) {
            $table->bigInteger("template_id")->unsigned()->nullable();
        });

        Schema::table('categories', function(Blueprint $table) {
            $table->foreign('template_id')->references('id')->on('templates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('categories', 'template_id')) {
            Schema::table('categories', function (Blueprint $table) {
                $table->dropForeign('template_id');
                $table->dropColumn('template_id');
            });
        }
    }
}
