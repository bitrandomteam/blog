<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeletePublishFieldFromTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tags', function (Blueprint $table) {
            $table->dropColumn("is_published");
            $table->dropColumn("publish_time");
            $table->dropColumn("unpublish_time");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('tags', 'is_published')) {
            Schema::table('tags', function (Blueprint $table) {
                $table->boolean("is_published")->default(0);
            });
        }

        if (!Schema::hasColumn('tags', 'publish_time')) {
            Schema::table('tags', function (Blueprint $table) {
                $table->timestamp('publish_time')->nullable();
            });
        }

        if (!Schema::hasColumn('tags', 'unpublish_time')) {
            Schema::table('tags', function (Blueprint $table) {
                $table->timestamp('unpublish_time')->nullable();
            });
        }
    }
}
