<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable("categories")) {
            Schema::create('categories', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('header', 255);
                $table->string('slug', 255);
                $table->string('seo_title', 255)->nullable();
                $table->string('seo_keywords', 500)->nullable();
                $table->string('seo_description', 500)->nullable();
                $table->string('description', 10000)->nullable();
                $table->timestamp('publish_time')->nullable();
                $table->timestamp('unpublish_time')->nullable();
                $table->boolean('is_published')->default(0);
                $table->bigInteger("parent_id")->unsigned()->nullable();
                $table->timestamps();

                $table->foreign("parent_id")->references("id")->on("categories");
            });
        }

        Schema::create('category_image', function (Blueprint $table) {
            $table->bigInteger('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

            $table->bigInteger('image_id')->unsigned()->nullable();
            $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');

             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_image');
    }
}
