<?php

use App\Category;
use App\Image;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Category::class, 'parents', 10)->create()->each(function ($category) {
            if (rand(0, 10)) {
                $category->images()->saveMany(Image::inRandomOrder()->take(1)->get());
            }
        });
        factory(Category::class, 'children', 50)->create()->each(function ($category) {
            if (rand(0, 20)) {
                $category->images()->saveMany(Image::inRandomOrder()->take(1)->get());
            }
        });
        echo "Категории сгенерированы\n";
    }
}
