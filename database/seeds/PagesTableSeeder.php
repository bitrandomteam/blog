<?php

use App\Template;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        DB::table("pages")->insert([
           [
                'header' => "Главная",
                'slug' => "main",
                'seo_title' => "Главная",
                'seo_keywords' => "блог, правильное питание, здоровье",
                'seo_description' => "добро пожаловать на мой блог о здоровье и правильном питании",
                'text' => "<p>" . $faker->paragraphs(2, true) . "</p>",
                'publish_time' => null,
                'unpublish_time' => null,
                'is_published' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'views' => 0,
                'template_id' => 7,
                'settings' => "{\"posts_count_on_page\":10,\"recent_posts_count\":4,\"featured_posts_count\":3,\"tags_count\":8,\"subcategories_count\":5,\"categories_count\":5}",
                'show_in_menu' => 1,
                'menu_ordering' => 0
           ],
            [
                'header' => "Категории",
                'slug' => "categories",
                'seo_title' => "Категории",
                'seo_keywords' => "блог, правильное питание, здоровье",
                'seo_description' => "добро пожаловать на мой блог о здоровье и правильном питании",
                'text' => "<p>" . $faker->paragraphs(4, true) . "</p>",
                'publish_time' => null,
                'unpublish_time' => null,
                'is_published' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'views' => 0,
                'template_id' => 5,
                'settings' => "{\"posts_count_on_page\":10,\"recent_posts_count\":4,\"featured_posts_count\":3,\"tags_count\":8,\"subcategories_count\":5,\"categories_count\":5}",
                'show_in_menu' => 1,
                'menu_ordering' => 1
            ],
            [
                'header' => "Теги",
                'slug' => "tags",
                'seo_title' => "Теги",
                'seo_keywords' => "блог, правильное питание, здоровье",
                'seo_description' => "добро пожаловать на мой блог о здоровье и правильном питании",
                'text' => "<p>" . $faker->paragraphs(5, true) . "</p>",
                'publish_time' => null,
                'unpublish_time' => null,
                'is_published' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'views' => 0,
                'template_id' => 6,
                'settings' => "{\"posts_count_on_page\":10,\"recent_posts_count\":4,\"featured_posts_count\":3,\"tags_count\":8,\"subcategories_count\":5,\"categories_count\":5}",
                'show_in_menu' => 1,
                'menu_ordering' => 2
            ],
            [
                'header' => "Обо мне",
                'slug' => "about",
                'seo_title' => "Обо мне",
                'seo_keywords' => "блог, правильное питание, здоровье",
                'seo_description' => "добро пожаловать на мой блог о здоровье и правильном питании",
                'text' => "<p>" . $faker->paragraphs(2, true) . "</p>",
                'publish_time' => null,
                'unpublish_time' => null,
                'is_published' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'views' => 0,
                'template_id' => 4,
                'settings' => "{\"posts_count_on_page\":10,\"recent_posts_count\":4,\"featured_posts_count\":3,\"tags_count\":8,\"subcategories_count\":5,\"categories_count\":5}",
                'show_in_menu' => 1,
                'menu_ordering' => 3
            ],
        ]);

        echo "Основные страницы созданы\n";
    }
}
