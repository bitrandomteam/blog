<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('images')->insert([
            [
                'header' => '55099fdeecad048b7ac7fbe9-750-422.jpg',
                'alt' => '55099fdeecad048b7ac7fbe9-750-422.jpg',
                'filepath' => 'images/55099fdeecad048b7ac7fbe9-750-422.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'header' => '84170952-b.jpg',
                'alt' => '84170952-b.jpg',
                'filepath' => 'images/84170952-b.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'header' => '983794168.jpg',
                'alt' => '983794168.jpg',
                'filepath' => 'images/983794168.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'header' => 'e8bf0aafda25be93e836612b281739db.jpg',
                'alt' => 'e8bf0aafda25be93e836612b281739db.jpg',
                'filepath' => 'images/e8bf0aafda25be93e836612b281739db.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'header' => 'Fast_Food_Dishes_for_Takeaway_in_Bar_4K_Wallpapers.jpg',
                'alt' => 'Fast_Food_Dishes_for_Takeaway_in_Bar_4K_Wallpapers.jpg',
                'filepath' => 'images/Fast_Food_Dishes_for_Takeaway_in_Bar_4K_Wallpapers.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'header' => 'Images_464257.jpg',
                'alt' => 'Images_464257.jpg',
                'filepath' => 'images/Images_464257.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'header' => 'img_forest.jpg',
                'alt' => 'img_forest.jpg',
                'filepath' => 'images/img_forest.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'header' => 'index.jpg',
                'alt' => 'index.jpg',
                'filepath' => 'images/index.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'header' => 'mediterranean-food-pasta-tomato-olives-garlic-uhd-4k-walpaper.jpg',
                'alt' => 'mediterranean-food-pasta-tomato-olives-garlic-uhd-4k-walpaper.jpg',
                'filepath' => 'images/mediterranean-food-pasta-tomato-olives-garlic-uhd-4k-walpaper.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'header' => 'nature-3082832_960_720.jpg',
                'alt' => 'nature-3082832_960_720.jpg',
                'filepath' => 'images/nature-3082832_960_720.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'header' => 'pexels-photo-376464.jpeg',
                'alt' => 'pexels-photo-376464.jpeg',
                'filepath' => 'images/pexels-photo-376464.jpeg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'header' => 'pexels-photo-461198.jpeg',
                'alt' => 'pexels-photo-461198.jpeg',
                'filepath' => 'images/pexels-photo-461198.jpeg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);

        echo "Изображения добавлены\n";
    }
}
