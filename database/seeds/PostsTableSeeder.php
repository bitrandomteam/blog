<?php

use App\Category;
use App\Image;
use App\Post;
use App\Tag;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Post::class, 500)->create()->each(function ($post) {
            if (rand(0, 20)) {
                $post->images()->saveMany(Image::inRandomOrder()->take(1)->get());
            }

            $tagsCount = rand(0, 6);
            $usedTags = [];
            for ($i = 0; $i < $tagsCount; $i++) {
                $tag = Tag::whereNotIn("id", $usedTags)->inRandomOrder()->first();
                $usedTags[] = $tag->id;

                $post->tags()->attach($tag);
            }
        });
        echo "Публикации сгенерированы\n";
    }
}
